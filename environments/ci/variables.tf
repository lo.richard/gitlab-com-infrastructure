variable "project" {
  default = "gitlab-ci-155816"
}

variable "region" {
  default = "us-east1"
}

variable "network" {
  default = "default"
}

# See 1password
variable "miner_ips" {
  type    = list(string)
  default = []
}

variable "bootstrap_script_version" {
  default = 9
}

variable "chef_provision" {
  type        = map(string)
  description = "Configuration details for chef server"

  default = {
    bootstrap_bucket  = "gitlab-ci-chef-bootstrap"
    bootstrap_key     = "gitlab-ci-bootstrap-validation"
    bootstrap_keyring = "gitlab-ci-bootstrap"
    server_url        = "https://chef.gitlab.com/organizations/gitlab/"
    user_name         = "gitlab-ci"
    user_key_path     = ".chef.pem"
    version           = "14.13.11"
  }
}

variable "dns_zone_name" {
  default = "gitlab.com"
}

variable "environment" {
  default = "ci"
}

variable "machine_types" {
  type = map(string)

  default = {
    "sd-exporter" = "n1-standard-1"
  }
}

variable "node_count" {
  type = map(string)

  default = {
    "sd-exporter" = 1
  }
}

variable "public_ports" {
  type = map(list(string))

  default = {
    "sd-exporter" = []
  }
}

variable "service_account_email" {
  type = string

  default = "terraform@gitlab-ci-155816.iam.gserviceaccount.com"
}

variable "gcs_service_account_email" {
  type    = string
  default = "gitlab-object-storage@gitlab-ci-155816.iam.gserviceaccount.com"
}

variable "gcs_storage_analytics_group_email" {
  type    = string
  default = "cloud-storage-analytics@google.com"
}
