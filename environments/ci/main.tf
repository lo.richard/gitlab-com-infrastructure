terraform {
  backend "s3" {
    bucket = "gitlab-com-infrastructure"
    key    = "terraform/ci/terraform.tfstate"
    region = "us-east-1"
  }
}

provider "google" {
  project = var.project
  region  = var.region
  version = "~> 2.14.0"
}

provider "random" {
  version = "~> 2.2"
}

##################################
#
#  Network
#
##################################

data "google_compute_network" "default_network" {
  name = "default"
}

resource "google_compute_subnetwork" "private_runners" {
  ip_cidr_range = "10.0.0.0/20"
  name          = "private-runners"
  network       = data.google_compute_network.default_network.self_link
  region        = var.region
}

resource "google_compute_subnetwork" "gitlab_shared_runners" {
  ip_cidr_range = "10.0.16.0/20"
  name          = "gitlab-shared-runners"
  network       = data.google_compute_network.default_network.self_link
  region        = var.region
}

resource "google_compute_subnetwork" "shared_runners" {
  ip_cidr_range = "10.0.32.0/20"
  name          = "shared-runners"
  network       = data.google_compute_network.default_network.self_link
  region        = var.region
}

##################################
#
#  NAT gateway
#
##################################

module "nat" {
  source = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/cloud-nat.git?ref=v1.0.0"

  log_level        = "ALL"
  nat_ip_count     = 40
  nat_ports_per_vm = 256
  region           = var.region

  # Right now there is only the "default" network in ci.
  # We should change this to module.network.name once we manage ci networks in terraform.
  network_name = "default"

  # Prevents change in NAT+IP name schema in v1.0.0, which would cause downtime.
  nat_name    = "nat-default-us-east1"
  router_name = "nat-router-default-us-east1"
}

##################################
#
#  Object storage buckets
#
##################################

# hp: I created the buckets using this module, but unfortunately it always fails
# to create "gitlab-ci-assets" and "gitlab-ci-secrets", as gcs bucket names need
# to be globally unique and apparantly those names already exist somewhere on the
# world. So I removed the buckets from the tf state again and this needs to stay
# commented.
#
#module "gitlab_object_storage" {
#  environment                       = var.environment
#  service_account_email             = var.service_account_email
#  gcs_service_account_email         = var.gcs_service_account_email
#  gcs_storage_analytics_group_email = var.gcs_storage_analytics_group_email
#  source                            = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/storage-buckets.git?ref=v5.0.0"
#}

# Create the bootstrap KMS key ring
resource "google_kms_key_ring" "bootstrap" {
  name     = "gitlab-${var.environment}-bootstrap"
  location = "global"
  project  = var.project
}

resource "google_kms_key_ring_iam_binding" "bootstrap" {
  key_ring_id = google_kms_key_ring.bootstrap.id
  role        = "roles/cloudkms.cryptoKeyDecrypter"

  members = [
    "serviceAccount:${var.service_account_email}",
  ]
}

# Create the gitlab-secrets KMS key ring
resource "google_kms_key_ring" "gitlab-secrets" {
  name     = "gitlab-secrets"
  location = "global"
  project  = var.project
}

resource "google_kms_key_ring_iam_binding" "secrets" {
  key_ring_id = google_kms_key_ring.gitlab-secrets.id
  role        = "roles/cloudkms.cryptoKeyDecrypter"

  members = [
    "serviceAccount:${var.service_account_email}",
  ]
}

##############################
#
# Monitoring
#
##############################

module "sd-exporter" {
  additional_scopes         = ["https://www.googleapis.com/auth/monitoring"]
  allow_stopping_for_update = true
  bootstrap_version         = var.bootstrap_script_version
  chef_provision            = var.chef_provision
  chef_run_list             = "\"role[${var.environment}-infra-sd-exporter]\""
  create_backend_service    = false
  dns_zone_name             = var.dns_zone_name
  environment               = var.environment
  machine_type              = var.machine_types["sd-exporter"]
  name                      = "sd-exporter"
  node_count                = var.node_count["sd-exporter"]
  project                   = var.project
  public_ports              = var.public_ports["sd-exporter"]
  region                    = var.region
  service_account_email     = var.service_account_email
  source                    = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.0.0"
  subnetwork_name           = "default"
  tier                      = "inf"
  use_new_node_name         = true
  use_external_ip           = true
  vpc                       = "default"
}

# We want only 1 logs-based metric per project, not per NAT. This way, the
# stackdriver metric (and exported prometheus metric) will have a static name,
# with different NAT instances being identified by the "nat_name" label. Note
# that Stackdriver already applies a "gateway_name" label so we don't need to
# add our own.
resource "google_logging_metric" "errors" {
  name = "nat-errors"

  filter = <<EOS
resource.type="nat_gateway"
AND jsonPayload.allocation_status="DROPPED"
EOS

  # counter
  metric_descriptor {
    metric_kind = "DELTA"
    value_type  = "INT64"
  }
}

resource "google_logging_metric" "translations" {
  name = "nat-translations"

  filter = <<EOS
resource.type="nat_gateway"
AND jsonPayload.allocation_status="OK"
EOS

  # counter
  metric_descriptor {
    metric_kind = "DELTA"
    value_type  = "INT64"
  }
}
