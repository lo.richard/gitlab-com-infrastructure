resource "google_compute_firewall" "block_miners" {
  name      = "block-miners"
  direction = "EGRESS"
  network   = var.network
  priority  = 1000

  target_tags        = ["docker-machine"]
  destination_ranges = var.miner_ips

  deny {
    protocol = "all"
  }
}

resource "google_compute_firewall" "docker_machine_deny_ingress" {
  name      = "docker-machine-block-ingress"
  direction = "INGRESS"
  network   = var.network
  priority  = 65500

  target_tags   = ["docker-machine"]
  source_ranges = ["0.0.0.0/0"]

  deny {
    protocol = "all"
  }
}

resource "google_compute_firewall" "docker_machines" {
  name      = "docker-machines"
  direction = "INGRESS"
  network   = var.network
  priority  = 1000

  target_tags   = ["docker-machine"]
  source_ranges = ["0.0.0.0/0"]

  allow {
    protocol = "tcp"
    ports    = [2376]
  }
}

resource "google_compute_firewall" "prometheus" {
  name      = "prometheus"
  direction = "INGRESS"
  network   = var.network
  priority  = 1000

  source_ranges = [
    "104.209.180.217/32",
    "104.209.189.215/32",
    "13.77.80.142/32",
    "40.70.72.145/32",
    "13.68.87.12/32",
    "52.225.221.89/32",
    "52.184.190.120/32",
    "35.227.108.10",
    "35.237.14.194",
    "35.185.16.254", # gitlab-production / prometheus-01-inf-gprd
    "34.74.136.38",  # gitlab-production / prometheus-02-inf-gprd
  ]

  allow {
    protocol = "tcp"
    ports    = [9090, 9100, 9402, 9145, 9393, 9000]
  }
}

resource "google_compute_firewall" "runners_cache" {
  name      = "runners-cache"
  direction = "INGRESS"
  network   = var.network
  priority  = 1000

  source_ranges = ["174.138.71.155/32"]
  target_tags   = ["runners-cache"]

  allow {
    protocol = "tcp"
    ports    = [443, 1443, 5000, 9000]
  }
}

resource "google_compute_firewall" "thanos" {
  name        = "thanos"
  description = "Prometheus Thanos access"
  direction   = "INGRESS"
  network     = var.network
  priority    = 1000

  target_tags = ["prometheus-server"]

  source_ranges = [
    "35.227.109.92/32",
    "35.237.131.211/32",
    "35.227.203.148/32",
    "130.211.36.217/32",
    "35.237.55.26/32",
    "35.237.254.196/32",
    "104.196.117.149",
    "35.231.229.167/32",
    "34.74.189.200/32",
  ]

  allow {
    protocol = "tcp"
    ports    = [10901, 10902]
  }
}

resource "google_compute_firewall" "windows_autoscaled_runner" {
  name      = "windows-autoscaled-runner"
  direction = "INGRESS"
  network   = var.network
  priority  = 1000

  target_tags   = ["windows-autoscaled-runner"]
  source_ranges = ["0.0.0.0/0"]

  allow {
    protocol = "tcp"
    ports    = [5985, 3389]
  }
}
