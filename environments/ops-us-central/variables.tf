variable "project" {
  default = "gitlab-ops"
}

variable "region" {
  default = "us-central1"
}

variable "environment" {
  default = "ops-us-central"
}

variable "dns_zone_name" {
  default = "gitlab.net"
}

variable "bootstrap_script_version" {
  default = 8
}

variable "oauth2_client_id_log_proxy" {
}

variable "oauth2_client_secret_log_proxy" {
}

variable "oauth2_client_id_dashboards" {
}

variable "oauth2_client_secret_dashboards" {
}

variable "oauth2_client_id_gitlab_ops" {
}

variable "oauth2_client_secret_gitlab_ops" {
}

variable "oauth2_client_id_monitoring" {
}

variable "oauth2_client_secret_monitoring" {
}

variable "machine_types" {
  type = map(string)

  default = {
    "alerts"                = "n1-standard-1"
    "aptly"                 = "n1-standard-1"
    "consul"                = "n1-standard-1"
    "log-proxy"             = "n1-standard-1"
    "proxy"                 = "n1-standard-1"
    "bastion"               = "n1-standard-1"
    "dashboards"            = "n1-standard-2"
    "dashboards-com"        = "n1-standard-4"
    "monitor"               = "n1-standard-8"
    "monitoring"            = "n1-standard-2"
    "gitlab-ops"            = "n1-standard-16"
    "runner-build"          = "n1-standard-32"
    "runner-chatops"        = "n1-standard-8"
    "runner-release"        = "n1-standard-8"
    "runner-release-single" = "n1-standard-1"
    "runner-snapshots"      = "n1-standard-1"
    "blackbox"              = "n1-standard-1"
    "sentry"                = "n1-standard-16"
    "sd-exporter"           = "n1-standard-1"
    "thanos-compact"        = "n1-standard-2"
    "thanos-query"          = "n1-standard-8"
    "thanos-store"          = "n1-highmem-8"
    "gke-runner"            = "n1-standard-2"
    "nessus"                = "n1-standard-4"
  }
}

variable "monitoring_hosts" {
  type = map(list(string))

  default = {
    "names" = ["alerts", "prometheus", "prometheus-app", "thanos-query"]
    "ports" = [9093, 9090, 9090, 10902]
  }
}

variable "service_account_email" {
  type = string

  default = "terraform@gitlab-ops.iam.gserviceaccount.com"
}

# The ops network is allocated
# 10.253.0.0/16

variable "subnetworks" {
  type = map(string)

  default = {
    "logging"          = "10.253.1.0/24"
    "bastion"          = "10.253.2.0/24"
    "dashboards"       = "10.253.3.0/24"
    "gitlab-ops"       = "10.253.4.0/24"
    "proxy"            = "10.253.5.0/24"
    "monitor"          = "10.253.6.0/24"
    "runner"           = "10.253.7.0/24"
    "monitoring"       = "10.253.8.0/24"
    "sentry"           = "10.253.9.0/24"
    "runner-chatops"   = "10.253.10.0/24"
    "dashboards-com"   = "10.253.11.0/24"
    "runner-release"   = "10.253.12.0/24"
    "gitlab-ops-geo"   = "10.253.13.0/24"
    "pubsubbeat"       = "10.253.14.0/24"
    "sd-exporter"      = "10.253.15.0/24"
    "gke-runner"       = "10.253.16.0/24"
    "runner-snapshots" = "10.253.17.0/24"
    "thanos-store"     = "10.253.18.0/24"
    "thanos-compact"   = "10.253.19.0/24"
    "aptly"            = "10.253.20.0/24"
    "consul"           = "10.253.21.0/24"
  }
}

variable "public_ports" {
  type = map(list(string))

  default = {
    "log-proxy"   = []
    "proxy"       = []
    "bastion"     = [22]
    "dashboards"  = []
    "gitlab-ops"  = [443, 80, 22, 5005]
    "pubsubbeat"  = []
    "runner"      = []
    "blackbox"    = []
    "sentry"      = [443, 80]
    "sd-exporter" = []
    "thanos"      = []
    "nessus"      = [8834]
    "aptly"       = [80]
    "consul"      = []
  }
}

variable "node_count" {
  type = map(string)

  default = {
    "gitlab-ops" = 1
    "runner"     = 1
  }
}

variable "chef_provision" {
  type        = map(string)
  description = "Configuration details for chef server"

  default = {
    bootstrap_bucket  = "gitlab-ops-chef-bootstrap"
    bootstrap_key     = "gitlab-ops-bootstrap-validation"
    bootstrap_keyring = "gitlab-ops-bootstrap"
    server_url        = "https://chef.gitlab.com/organizations/gitlab/"
    user_name         = "gitlab-ci"
    user_key_path     = ".chef.pem"
    version           = "14.13.11"
  }
}

variable "monitoring_cert_link" {
  default = "projects/gitlab-ops/global/sslCertificates/wildcard-ops-gitlab-net"
}

variable "lb_fqdns_bastion" {
  type    = list(string)
  default = ["lb-bastion.ops.gitlab.com"]
}

variable "network_testbed" {
  default = "https://www.googleapis.com/compute/v1/projects/gitlab-testbed/global/networks/testbed"
}

variable "network_ops" {
  default = "https://www.googleapis.com/compute/v1/projects/gitlab-ops/global/networks/ops"
}

variable "network_ops_us_central" {
  default = "https://www.googleapis.com/compute/v1/projects/gitlab-ops/global/networks/ops-us-central"
}

variable "network_gprd" {
  default = "https://www.googleapis.com/compute/v1/projects/gitlab-production/global/networks/gprd"
}

variable "network_gstg" {
  default = "https://www.googleapis.com/compute/v1/projects/gitlab-staging-1/global/networks/gstg"
}

variable "network_dr" {
  default = "https://www.googleapis.com/compute/v1/projects/gitlab-dr/global/networks/dr"
}

variable "network_pre" {
  default = "https://www.googleapis.com/compute/v1/projects/gitlab-pre/global/networks/pre"
}

variable "tcp_lbs_bastion" {
  type = map(list(string))

  default = {
    "names"                  = ["ssh"]
    "forwarding_port_ranges" = ["22"]
    "health_check_ports"     = ["80"]
  }
}

variable "tcp_lbs_sentry" {
  type = map(list(string))

  default = {
    "names"                      = ["http", "https"]
    "forwarding_port_ranges"     = ["80", "443"]
    "health_check_ports"         = ["9000", "9000"]
    "health_check_request_paths" = ["/auth/login/gitlab/", "/auth/login/gitlab/"]
  }
}

variable "tcp_lbs_aptly" {
  type = map(list(string))

  default = {
    "names"                  = ["http", "https"]
    "forwarding_port_ranges" = ["80", "443"]
    "health_check_ports"     = ["80", "80"]
  }
}

variable "log_gitlab_net_cert_link" {
  default = "projects/gitlab-ops/global/sslCertificates/log-gitlab-net"
}

variable "ops_gitlab_net_cert_link" {
  default = "projects/gitlab-ops/global/sslCertificates/ops-gitlab-net"
}

variable "dashboards_gitlab_net_cert_link" {
  default = "projects/gitlab-ops/global/sslCertificates/dashboards-gitlab-net-2019"
}

variable "dashboards_gitlab_com_cert_link" {
  default = "projects/gitlab-ops/global/sslCertificates/dashboards-gitlab-com"
}

variable "gcs_service_account_email" {
  type    = string
  default = "gitlab-object-storage@gitlab-ops.iam.gserviceaccount.com"
}

# Service account used to do automated backup testing
# in https://gitlab.com/gitlab-restore/postgres-gprd

variable "gcs_postgres_backup_service_account" {
  type    = string
  default = "postgres-wal-archive@gitlab-ops.iam.gserviceaccount.com"
}

variable "gcs_postgres_restore_service_account" {
  type    = string
  default = "postgres-automated-backup-test@gitlab-restore.iam.gserviceaccount.com"
}

variable "gcs_postgres_backup_kms_key_id" {
  type    = string
  default = "projects/gitlab-ops/locations/global/keyRings/gitlab-secrets/cryptoKeys/ops-postgres-wal-archive"
}

variable "postgres_backup_retention_days" {
  type    = string
  default = "5"
}

#######################
# pubsubbeat config
#######################

variable "pubsubbeats" {
  type = map(list(string))

  default = {
    "names"         = ["gitaly", "haproxy", "pages", "postgres", "production", "system", "workhorse", "rspec", "sidekiq", "api", "nginx", "gitlab-shell", "shell", "rails", "unstructured", "unicorn", "application", "registry", "redis", "consul", "runner"]
    "machine_types" = ["n1-standard-2", "n1-standard-2", "n1-standard-2", "n1-standard-2", "n1-standard-2", "n1-standard-2", "n1-standard-2", "n1-standard-2", "n1-standard-2", "n1-standard-2", "n1-standard-2", "n1-standard-2", "n1-standard-2", "n1-standard-2", "n1-standard-2", "n1-standard-1", "n1-standard-1", "n1-standard-1", "n1-standard-1", "n1-standard-1", "n1-standard-1"]
  }
}

### Object Storage Configuration

variable "versioning" {
  type    = string
  default = "true"
}

variable "artifact_age" {
  type    = string
  default = "30"
}

variable "upload_age" {
  type    = string
  default = "30"
}

variable "lfs_object_age" {
  type    = string
  default = "30"
}

variable "package_repo_age" {
  type    = string
  default = "30"
}

variable "storage_class" {
  type    = string
  default = "MULTI_REGIONAL"
}

variable "storage_log_age" {
  type    = string
  default = "7"
}

variable "gcs_storage_analytics_group_email" {
  type    = string
  default = "cloud-storage-analytics@google.com"
}

