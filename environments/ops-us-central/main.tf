## State storage
terraform {
  backend "s3" {
    bucket = "gitlab-com-infrastructure"
    key    = "terraform/ops-us-central/terraform.tfstate"
    region = "us-east-1"
  }
}

data "terraform_remote_state" "ops" {
  backend = "s3"

  config = {
    bucket = "gitlab-com-infrastructure"
    key    = "terraform/ops/terraform.tfstate"
    region = "us-east-1"
  }
}

## AWS
provider "aws" {
  version = "~> 2.23.0"

  region = "us-east-1"
}

variable "gitlab_com_zone_id" {
}

variable "gitlab_net_zone_id" {
}

## Google

provider "google" {
  version = "~> 2.14.0"
  project = var.project
  region  = var.region
}

##################################
#
#  Network
#
#################################

module "network" {
  # TODO Migrate this environment to v2.0.0+ (https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/7860)
  source              = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/vpc.git?ref=craigf/v1.1.3-tf0.12"
  project             = var.project
  environment         = var.environment
  global_address_name = "ops-us-central-service-network"
}

##################################
#
#  Network Peering
#
#################################

resource "google_compute_network_peering" "peering_gprd" {
  name         = "peering-gprd"
  network      = var.network_ops_us_central
  peer_network = var.network_gprd
}

resource "google_compute_network_peering" "peering_gstg" {
  name         = "peering-gstg"
  network      = var.network_ops_us_central
  peer_network = var.network_gstg
}

resource "google_compute_network_peering" "peering_dr" {
  name         = "peering-dr"
  network      = var.network_ops_us_central
  peer_network = var.network_dr
}

resource "google_compute_network_peering" "peering_pre" {
  name         = "peering-pre"
  network      = var.network_ops_us_central
  peer_network = var.network_pre
}

resource "google_compute_network_peering" "peering_testbed" {
  name         = "peering-testbed"
  network      = var.network_ops_us_central
  peer_network = var.network_testbed
}

resource "google_compute_network_peering" "peering_ops" {
  name         = "peering-ops"
  network      = var.network_ops_us_central
  peer_network = var.network_ops
}

#######################################################
#
# Cloud Services for ops.gitlab.net
#
#######################################################

data "null_data_source" "ops-public-ip" {
  inputs = {
    name  = "ops-gitlab-net"
    value = data.terraform_remote_state.ops.outputs.ops_ip
  }
}

data "null_data_source" "ops-us-central-public-ip" {
  inputs = {
    name  = "ops-gitlab-net-us-central"
    value = module.gitlab-ops.instances.0.network_interface.0.access_config.0.nat_ip
  }
}

resource "random_id" "db_name_suffix" {
  byte_length = 4
}

resource "google_sql_database_instance" "ops-gitlab-net" {
  name             = "ops-gitlab-net-${random_id.db_name_suffix.hex}"
  database_version = "POSTGRES_11"
  region           = var.region

  settings {
    tier              = "db-custom-4-26624"
    availability_type = "REGIONAL"

    ip_configuration {
      ipv4_enabled    = "true"
      private_network = module.network.self_link

      dynamic "authorized_networks" {
        for_each = [data.null_data_source.ops-public-ip.outputs]
        content {
          # TF-UPGRADE-TODO: The automatic upgrade tool can't predict
          # which keys might be set in maps assigned here, so it has
          # produced a comprehensive set here. Consider simplifying
          # this after confirming which keys can be set in practice.

          expiration_time = lookup(authorized_networks.value, "expiration_time", null)
          name            = lookup(authorized_networks.value, "name", null)
          value           = lookup(authorized_networks.value, "value", null)
        }
      }
      dynamic "authorized_networks" {
        for_each = [data.null_data_source.ops-us-central-public-ip.outputs]
        content {
          # TF-UPGRADE-TODO: The automatic upgrade tool can't predict
          # which keys might be set in maps assigned here, so it has
          # produced a comprehensive set here. Consider simplifying
          # this after confirming which keys can be set in practice.

          expiration_time = lookup(authorized_networks.value, "expiration_time", null)
          name            = lookup(authorized_networks.value, "name", null)
          value           = lookup(authorized_networks.value, "value", null)
        }
      }
    }

    maintenance_window {
      day          = "6"
      hour         = "23"
      update_track = "stable"
    }

    backup_configuration {
      enabled = "true"
    }
  }
}

#######################################################
#
# VM for ops.gitlab.net
#
#######################################################

module "gitlab-ops" {
  backend_protocol      = "HTTPS"
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-infra-gitlab-secondary]\""
  data_disk_size        = 10
  data_disk_type        = "pd-standard"
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  health_check          = "http"
  health_check_port     = 8887
  ip_cidr_range         = var.subnetworks["gitlab-ops"]
  machine_type          = var.machine_types["gitlab-ops"]
  name                  = "gitlab"
  node_count            = var.node_count["gitlab-ops"]
  oauth2_client_id      = var.oauth2_client_id_gitlab_ops
  oauth2_client_secret  = var.oauth2_client_secret_gitlab_ops
  persistent_disk_path  = "/mnt/unused"
  project               = var.project
  public_ports          = var.public_ports["gitlab-ops"]
  region                = var.region
  service_account_email = var.service_account_email
  service_path          = "/-/liveness"
  service_port          = 443
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/monitoring-with-count.git?ref=v3.0.0"
  tier                  = "inf"
  use_external_ip       = true
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Runner
#
##################################

module "runner" {
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-runner-build]\""
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["runner"]
  machine_type          = var.machine_types["runner-build"]
  name                  = "runner"
  node_count            = var.node_count["runner"]
  os_disk_size          = 500
  project               = var.project
  public_ports          = var.public_ports["runner"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.0.0"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Runner for ChatOps
#
##################################

module "runner-chatops" {
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-runner-chatops]\""
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["runner-chatops"]
  machine_type          = var.machine_types["runner-chatops"]
  name                  = "runner-chatops"
  node_count            = var.node_count["runner"]
  os_disk_size          = 100
  project               = var.project
  public_ports          = var.public_ports["runner"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.0.0"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Runner for Release
#
##################################

module "runner-release" {
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-runner-release]\""
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["runner-release"]
  machine_type          = var.machine_types["runner-release"]
  name                  = "runner-release"
  node_count            = var.node_count["runner"]
  os_disk_size          = 100
  project               = var.project
  public_ports          = var.public_ports["runner"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.0.0"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

# This is a special release runner with a concurrency of 1 so
# that we can enforce single pipeline execution. This prevents
# multiple release pipelines from deploying simultaneously.
#
# This runner is used for the first job and the regular release
# release number (with high concurrency) is used for the later
# stages where there are many jobs in parallel.

module "runner-release-single" {
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-runner-release-single]\""
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["runner-release"]
  machine_type          = var.machine_types["runner-release-single"]
  name                  = "runner-release-single"
  node_count            = var.node_count["runner"]
  os_disk_size          = 100
  project               = var.project
  public_ports          = var.public_ports["runner"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.0.0"
  subnetwork_name       = module.runner-release.google_compute_subnetwork_name
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Runner for drive snapshot creation and restoring
#
##################################

module "runner-snapshots" {
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-runner-snapshots]\""
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["runner-snapshots"]
  machine_type          = var.machine_types["runner-snapshots"]
  name                  = "runner-snapshots"
  node_count            = var.node_count["runner"]
  os_disk_size          = 100
  project               = var.project
  public_ports          = var.public_ports["runner"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.0.0"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}
