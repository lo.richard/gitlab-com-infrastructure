// Configure the provider using the terraform admin project
// Reference: https://cloud.google.com/community/tutorials/managing-gcp-projects-with-terraform
provider "google" {
  project = "env-zero"
  region  = "us-east1"
  version = "~> 2.14.0"
}

// Use Terraform Remote State backed by Google Cloud Storage
terraform {
  backend "gcs" {
    bucket = "gitlab-com-infrastructure"
    prefix = "env-projects/terraform-states"
  }
}

// Create the projects
module "gitlab-ci" {
  source = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/project.git?ref=v6.2.0"

  api_services = distinct(
    concat(
      local.api_services["common"],
      local.api_services["gitlab-ci"],
    ),
  )
  billing_account    = var.billing_account
  bucket_name_prefix = "155816"
  project            = "gitlab-ci"
  project_id         = "gitlab-ci-155816"
  project_folder     = local.top_level_project_folder
}

module "gitlab-ci-windows" {
  source = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/project.git?ref=v6.2.0"

  api_services = distinct(
    concat(
      local.api_services["common"],
      local.api_services["gitlab-ci"],
    ),
  )
  billing_account    = "00435B-F3E15F-7AF1AB"
  bucket_name_prefix = "155816"
  project            = "gitlab-ci-windows"
  project_id         = "gitlab-ci-windows"
  project_folder     = local.top_level_project_folder
}

module "gitlab-dr" {
  source = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/project.git?ref=v6.2.0"

  api_services = distinct(
    concat(
      local.api_services["common"],
      local.api_services["gitlab-dr"],
    ),
  )
  billing_account    = var.billing_account
  bucket_name_prefix = ""
  project            = "gitlab-dr"
  project_folder     = local.project_folder
  use_name_as_id     = "true"
}

module "gitlab-ops" {
  source = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/project.git?ref=v6.2.0"

  api_services = distinct(
    concat(
      local.api_services["common"],
      local.api_services["gitlab-ops"],
    ),
  )
  billing_account    = var.billing_account
  bucket_name_prefix = ""
  project            = "gitlab-ops"
  project_folder     = local.top_level_project_folder // gitlab.com/Infrastructure
  use_name_as_id     = "true"
}

module "gitlab-pre" {
  source = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/project.git?ref=v6.2.0"

  api_services = distinct(
    concat(
      local.api_services["common"],
      local.api_services["gitlab-pre"],
    ),
  )
  billing_account    = var.billing_account
  bucket_name_prefix = ""
  project            = "gitlab-pre"
  project_folder     = local.project_folder
  use_name_as_id     = "true"
}

module "gitlab-production" {
  source = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/project.git?ref=v6.2.0"

  api_services = distinct(
    concat(
      local.api_services["common"],
      local.api_services["gitlab-production"],
    ),
  )
  billing_account            = var.billing_account
  bucket_name_prefix         = ""
  project                    = "gitlab-production"
  project_folder             = local.project_folder
  serial_port_logging_enable = true
  use_name_as_id             = "true"
}

module "gitlab-staging" {
  source = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/project.git?ref=v6.2.0"

  api_services = distinct(
    concat(
      local.api_services["common"],
      local.api_services["gitlab-staging"],
    ),
  )
  billing_account            = var.billing_account
  bucket_name_prefix         = ""
  project                    = "gitlab-staging"
  project_id                 = "gitlab-staging-1"
  project_folder             = local.project_folder
  serial_port_logging_enable = true
}

module "gitlab-testbed" {
  source = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/project.git?ref=v6.2.0"

  api_services = distinct(
    concat(
      local.api_services["common"],
      local.api_services["gitlab-testbed"],
    ),
  )
  billing_account    = var.billing_account
  bucket_name_prefix = ""
  project            = "gitlab-testbed"
  project_folder     = local.project_folder
  use_name_as_id     = "true"
}


