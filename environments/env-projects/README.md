# env-projects

## What is this?

A terraform environment for managing the pieces needed to manage other terraform
environments. These environments correspond 1:1 with GCP projects.

## Adding a new environment

1. Declare an instance of the Google project module.
1. When happy, apply the plan.
1. Manually generate a service account key for the terraform-ci service account,
   and add it to 1password. This can be used as a CI variable in order to plan
   the new project.
1. You should now be able to use this project in a Google terraform provider
   declaration in a new environment under
   `<gitlab-com-infrastructure>/environments/new-env`.
1. Carry on with the "Creating a new environment" instructions in the root
   readme of this project.

## Importing an existing environment

1. Declare an instance of the Google project module.
1. `tf plan` to see the resources.
1. We must create the random ID to avoid bogus plan diffs on the project ID
   later:

   ```
   tf apply -target module.new-env.random_id.id
   ```

1. Import the existing Google project and its services:

   ```
   tf import module.new-env.google_project.project new-project-id
   tf import module.new-env.google_project_services.api_services new-project-id
   ```

1. There should now be no plan diff for the project itself. There may be one for
   the project services. In this case, populate the list of project-specific
   (non-common) services in a similar way to that seen for the other projects.
1. When happy, apply the plan.
1. Manually generate a service account key for the terraform-ci service account,
   and add it to 1password. This can be used as a CI variable in order to plan
   the new project.
1. Carry on with the "Creating a new environment" instructions in the root
   readme of this project.
