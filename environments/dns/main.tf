## State storage
terraform {
  backend "s3" {
    bucket = "gitlab-com-infrastructure"
    key    = "terraform/dns/terraform.tfstate"
    region = "us-east-1"
  }
}

## AWS
provider "aws" {
  region = "us-east-1"
}

locals {
  zones = {
    "gitlab.org." = "${var.gitlab_org_zone_id}"
    "gitlab.io."  = "${var.gitlab_io_zone_id}"
    "gitlab.com." = "${var.gitlab_com_zone_id}"
    "gitlap.com." = "${var.gitlap_com_zone_id}"
    "gitlab.net." = "${var.gitlab_net_zone_id}"
  }
}

resource "aws_route53_record" "a" {
  for_each = merge(var.gitlab_org_a, var.gitlab_net_a, var.gitlab_io_a, var.gitlap_com_a, var.gitlab_com_a)

  zone_id = "${local.zones[regex("[\\w-]+\\.[\\w-]+.$", each.key)]}"
  name    = each.key
  type    = "A"
  ttl     = each.value.ttl
  records = each.value.records
}

resource "aws_route53_record" "aaaa" {
  for_each = merge(var.gitlab_com_aaaa)

  zone_id = "${local.zones[regex("[\\w-]+\\.[\\w-]+.$", each.key)]}"
  name    = each.key
  type    = "AAAA"
  ttl     = each.value.ttl
  records = each.value.records
}

resource "aws_route53_record" "caa" {
  for_each = merge(var.gitlab_com_caa, var.gitlab_org_caa, var.gitlap_com_caa, var.gitlab_net_caa)

  zone_id = "${local.zones[regex("[\\w-]+\\.[\\w-]+.$", each.key)]}"
  name    = each.key
  type    = "CAA"
  ttl     = each.value.ttl
  records = each.value.records
}

resource "aws_route53_record" "cname" {
  for_each = merge(var.gitlab_org_cname, var.gitlab_net_cname, var.gitlab_io_cname, var.gitlap_com_cname, var.gitlab_com_cname)

  zone_id = "${local.zones[regex("[\\w-]+\\.[\\w-]+.$", each.key)]}"
  name    = each.key
  type    = "CNAME"
  ttl     = each.value.ttl
  records = each.value.records
}

resource "aws_route53_record" "txt" {
  for_each = merge(var.gitlab_org_txt, var.gitlab_net_txt, var.gitlab_io_txt, var.gitlap_com_txt, var.gitlab_com_txt)

  zone_id = "${local.zones[regex("[\\w-]+\\.[\\w-]+.$", each.key)]}"
  name    = each.key
  type    = "TXT"
  ttl     = each.value.ttl
  records = each.value.records
}

resource "aws_route53_record" "ns" {
  for_each = merge(var.gitlab_org_ns, var.gitlab_net_ns, var.gitlab_io_ns, var.gitlap_com_ns, var.gitlab_com_ns)

  zone_id = "${local.zones[regex("[\\w-]+\\.[\\w-]+.$", each.key)]}"
  name    = each.key
  type    = "NS"
  ttl     = each.value.ttl
  records = each.value.records
}

resource "aws_route53_record" "mx" {
  for_each = merge(var.gitlab_org_mx, var.gitlab_net_mx, var.gitlab_io_mx, var.gitlap_com_mx, var.gitlab_com_mx)

  zone_id = "${local.zones[regex("[\\w-]+\\.[\\w-]+.$", each.key)]}"
  name    = each.key
  type    = "MX"
  ttl     = each.value.ttl
  records = each.value.records
}

resource "aws_route53_record" "soa" {
  for_each = merge(var.gitlab_org_soa, var.gitlab_net_soa, var.gitlab_io_soa, var.gitlap_com_soa, var.gitlab_com_soa)

  zone_id = "${local.zones[regex("[\\w-]+\\.[\\w-]+.$", each.key)]}"
  name    = each.key
  type    = "SOA"
  ttl     = each.value.ttl
  records = each.value.records
}

resource "aws_route53_record" "alias" {
  for_each = merge(var.gitlab_net_alias)

  zone_id = "${local.zones[regex("[\\w-]+\\.[\\w-]+.$", each.key)]}"
  name    = each.key
  type    = each.value.type

  alias {
    name                   = each.value.alias.name
    zone_id                = each.value.alias.zone_id
    evaluate_target_health = each.value.alias.evaluate_target_health
  }
}
