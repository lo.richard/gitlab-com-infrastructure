## State storage
terraform {
  backend "s3" {
    bucket = "gitlab-com-infrastructure"
    key    = "terraform/redirects/terraform.tfstate"
    region = "us-east-1"
  }
}

## AWS
provider "aws" {
  region = "us-east-1"
}

locals {
  zones = {
    "gitlab.org" = "${var.gitlab_org_zone_id}"
    "gitlab.io"  = "${var.gitlab_io_zone_id}"
    "gitlab.com"  = "${var.gitlab_com_zone_id}"
    "gitlap.com"  = "${var.gitlap_com_zone_id}"
  }
}

resource "aws_route53_record" "default" {
  for_each = var.redirects

  zone_id = "${local.zones[regex("[\\w-]+\\.[\\w-]+$", each.key)]}"
  name    = each.key
  type    = "CNAME"
  ttl     = "300"
  records = ["${lookup(var.tls_domains, each.key, "nonssl.global.fastly.net")}"]
}

resource "aws_route53_record" "a_records" {
  for_each = var.apex_redirects

  zone_id = "${local.zones[regex("[\\w-]+\\.[\\w-]+$", each.key)]}"
  name    = each.key
  type    = "A"
  ttl     = "300"
  records = var.tls_apex_domains_ips[each.key]
}

resource "fastly_service_v1" "redirects" {
  name = "Domain redirects"

  // A dummy backend, all responses from this service are synthetic.
  backend {
    name    = "dummy 127.0.0.1"
    address = "127.0.0.1"
    port    = 80
  }

  dynamic "domain" {
    for_each = merge(var.redirects, var.apex_redirects)

    content {
      name = domain.key
    }
  }

  dynamic "condition" {
    for_each = merge(var.redirects, var.apex_redirects)

    content {
      name      = "${condition.key} request"
      statement = "req.http.Host == \"${condition.key}\""
      type      = "REQUEST"
      priority  = 100
    }
  }

  dynamic "condition" {
    for_each = merge(var.redirects, var.apex_redirects)

    content {
      name      = "${condition.key} response"
      statement = "req.http.Host == \"${condition.key}\""
      type      = "RESPONSE"
      priority  = 100
    }
  }

  dynamic "response_object" {
    for_each = merge(var.redirects, var.apex_redirects)

    content {
      name              = "${response_object.key} response"
      status            = 301
      response          = "Moved Permanently"
      request_condition = "${response_object.key} request"
    }
  }

  dynamic "header" {
    for_each = merge(var.redirects, var.apex_redirects)

    content {
      name               = "${header.key} location"
      action             = "set"
      type               = "response"
      destination        = "http.Location"
      source             = "\"${header.value}\""
      response_condition = "${header.key} response"
    }
  }
}
