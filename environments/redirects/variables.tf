variable "gitlab_org_zone_id" {}
variable "gitlab_io_zone_id" {}
variable "gitlab_com_zone_id" {}
variable "gitlap_com_zone_id" {}

variable "redirects" {
  type = "map"

  default = {
    "api.gitlab.org" = "http://doc.gitlab.com/ce/api/"
    "www.gitlab.org" = "https://about.gitlab.com/"
    "www.gitlab.io" = "https://about.gitlab.com/"
    "blog.gitlab.com" = "https://about.gitlab.com/blog/"
    "blue-moon.gitlap.com" = "https://about.gitlab.com/"
  }
}

variable "apex_redirects" {
  type = "map"

  default = {
    "gitlab.org" = "https://about.gitlab.com/"
    "gitlab.io"  = "https://about.gitlab.com/"
  }
}

# For now you have to manually create the TLS domains you want to support in
# Fastly and specify here the corresponding certificate host. If you don't need
# to support TLS you can ommit the entry and we'll use nonssl.global.fastly.net
# by default
variable "tls_domains" {
  type = "map"

  default = {
    "www.gitlab.org" = "h3.shared.global.fastly.net"
    "www.gitlab.io" = "h3.shared.global.fastly.net"
    "api.gitlab.org" = "h3.shared.global.fastly.net"
    "blog.gitlab.com" = "h3.shared.global.fastly.net"
    "blue-moon.gitlap.com" = "m2.shared.global.fastly.net"
  }
}

variable "tls_apex_domains_ips" {
  type = "map"

  default = {
    "gitlab.org" = [
      "151.101.2.49",
      "151.101.66.49",
      "151.101.194.49",
      "151.101.130.49"
    ]
    "gitlab.io" = [
      "151.101.2.49",
      "151.101.66.49",
      "151.101.194.49",
      "151.101.130.49"
    ]
  }
}
