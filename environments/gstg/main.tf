## State storage
terraform {
  backend "s3" {
    bucket = "gitlab-com-infrastructure"
    key    = "terraform/gstg/terraform.tfstate"
    region = "us-east-1"
  }
}

## AWS
provider "aws" {
  region  = "us-east-1"
  version = "~> 2.27.0"
}

## Local
provider "local" {
  version = "~> 1.3.0"
}

## Google
provider "google" {
  project = var.project
  region  = var.region
  version = "~> 2.14.0"
}

provider "random" {
  version = "~> 2.2"
}

resource "google_project_iam_member" "serviceAccountTokenCreator" {
  project = var.project
  role    = "roles/iam.serviceAccountTokenCreator"
  member  = "serviceAccount:${var.service_account_email}"
}

resource "google_project_iam_member" "serviceAccountUser" {
  project = var.project
  role    = "roles/iam.serviceAccountUser"
  member  = "serviceAccount:${var.service_account_email}"
}

resource "google_project_iam_member" "logging_logWriter" {
  project = var.project
  role    = "roles/logging.logWriter"
  member  = "serviceAccount:${var.service_account_email}"
}

resource "google_project_iam_member" "pubsub_editor" {
  project = var.project
  role    = "roles/pubsub.editor"
  member  = "serviceAccount:${var.service_account_email}"
}

resource "google_project_iam_member" "pubsub_publisher" {
  project = var.project
  role    = "roles/pubsub.publisher"
  member  = "serviceAccount:${var.service_account_email}"
}

resource "google_project_iam_member" "pubsub_subscriber" {
  project = var.project
  role    = "roles/pubsub.subscriber"
  member  = "serviceAccount:${var.service_account_email}"
}

##################################
#
#  NAT gateway
#
#################################

module "nat" {
  source = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/cloud-nat.git?ref=v1.0.0"

  log_level    = "ALL"
  nat_ip_count = 2
  network_name = module.network.name
  region       = var.region

  # A NAT instance with this name was initially created for GKE's subnet. Later,
  # it was decided to use 1 Cloud NAT instance per region rather than per
  # subnet, for simplicity. Cloud NAT resources cannot have their name changed
  # without recreation, and 2 NATs cannot overlap in coverage (e.g. 1
  # subnet-specific NAT coexisting with 1 regional NAT). Therefore changing the
  # NAT or router name would cause downtime. This is why in our main
  # environments the Cloud NAT resource is called "gitlab-gke".
  nat_name    = "gitlab-gke"
  router_name = "gitlab-gke"

  # Prevents change in IP name schema in v1.0.0, which would cause downtime.
  ip_name_prefix = "nat-gstg-us-east1"
}

# We want only 1 logs-based metric per project, not per NAT. This way, the
# stackdriver metric (and exported prometheus metric) will have a static name,
# with different NAT instances being identified by the "nat_name" label. Note
# that Stackdriver already applies a "gateway_name" label so we don't need to
# add our own.
resource "google_logging_metric" "errors" {
  name = "nat-errors"

  filter = <<EOS
resource.type="nat_gateway"
AND jsonPayload.allocation_status="DROPPED"
EOS

  # counter
  metric_descriptor {
    metric_kind = "DELTA"
    value_type  = "INT64"
  }
}

resource "google_logging_metric" "translations" {
  name = "nat-translations"

  filter = <<EOS
resource.type="nat_gateway"
AND jsonPayload.allocation_status="OK"
EOS

  # counter
  metric_descriptor {
    metric_kind = "DELTA"
    value_type  = "INT64"
  }
}

##################################
#
#  Network
#
#################################

module "network" {
  environment      = var.environment
  project          = var.project
  source           = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/vpc.git?ref=v2.0.0"
  internal_subnets = var.internal_subnets
}

##################################
#
#  Network Peering
#
#################################

resource "google_compute_network_peering" "peering" {
  count        = length(var.peer_networks["names"])
  name         = "peering-${element(var.peer_networks["names"], count.index)}"
  network      = var.network_env
  peer_network = element(var.peer_networks["links"], count.index)
}

##################################
#
#  Web front-end
#
#################################

module "web" {
  assign_public_ip      = false
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-fe-web]\""
  dns_zone_name         = var.dns_zone_name
  egress_ports          = var.web_egress_ports
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["web"]
  machine_type          = var.machine_types["web"]
  name                  = "web"
  node_count            = var.node_count["web"]
  os_disk_type          = "pd-ssd"
  project               = var.project
  public_ports          = var.public_ports["web"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 443
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.0.0"
  tier                  = "sv"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Deploy Canary
#
##################################

module "deploy-cny" {
  assign_public_ip      = false
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-deploy-node-cny]\""
  dns_zone_name         = var.dns_zone_name
  egress_ports          = var.deploy_egress_ports
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["deploy-cny"]
  machine_type          = var.machine_types["deploy"]
  name                  = "deploy-cny"
  node_count            = var.node_count["deploy-cny"]
  project               = var.project
  public_ports          = var.public_ports["deploy"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.0.0"
  tier                  = "sv"
  use_external_ip       = false
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Web Canary front-end
#
#################################

module "web-cny" {
  assign_public_ip      = false
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-fe-web-cny]\""
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["web"]
  machine_type          = var.machine_types["web"]
  name                  = "web-cny"
  node_count            = var.node_count["web-cny"]
  os_disk_type          = "pd-ssd"
  project               = var.project
  public_ports          = var.public_ports["web"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 443
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.0.0"
  subnetwork_name       = module.web.google_compute_subnetwork_name
  tier                  = "sv"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  API Canary front-end
#
#################################

module "api-cny" {
  assign_public_ip      = false
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-fe-api-cny]\""
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["api"]
  machine_type          = var.machine_types["api"]
  name                  = "api-cny"
  node_count            = var.node_count["api-cny"]
  os_disk_type          = "pd-ssd"
  project               = var.project
  public_ports          = var.public_ports["api"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 443
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.0.0"
  subnetwork_name       = module.api.google_compute_subnetwork_name
  tier                  = "sv"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Git Canary front-end
#
#################################

module "git-cny" {
  assign_public_ip      = false
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-fe-git-cny]\""
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["git"]
  machine_type          = var.machine_types["git"]
  name                  = "git-cny"
  node_count            = var.node_count["git-cny"]
  os_disk_type          = "pd-ssd"
  project               = var.project
  public_ports          = var.public_ports["git"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 443
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.0.0"
  subnetwork_name       = module.git.google_compute_subnetwork_name
  tier                  = "sv"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  API
#
#################################

module "api" {
  assign_public_ip      = false
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-fe-api]\""
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["api"]
  machine_type          = var.machine_types["api"]
  name                  = "api"
  node_count            = var.node_count["api"]
  os_disk_type          = "pd-ssd"
  project               = var.project
  public_ports          = var.public_ports["api"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 443
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.0.0"
  tier                  = "sv"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Git
#
##################################

module "git" {
  assign_public_ip      = false
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-fe-git]\""
  dns_zone_name         = var.dns_zone_name
  egress_ports          = var.egress_ports
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["git"]
  machine_type          = var.machine_types["git"]
  name                  = "git"
  node_count            = var.node_count["git"]
  os_disk_type          = "pd-ssd"
  project               = var.project
  public_ports          = var.public_ports["git"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.0.0"
  tier                  = "sv"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Pages web front-end
#
#################################

module "web-pages" {
  assign_public_ip      = false
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-fe-web-pages]\""
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["web-pages"]
  machine_type          = var.machine_types["web-pages"]
  name                  = "web-pages"
  node_count            = var.node_count["web-pages"]
  os_disk_type          = "pd-ssd"
  project               = var.project
  public_ports          = var.public_ports["web-pages"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 443
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.0.0"
  tier                  = "sv"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Database
#
#################################

module "postgres-dr-archive" {
  assign_public_ip      = false
  bootstrap_version     = var.bootstrap_script_version
  chef_init_run_list    = "\"recipe[gitlab-server::hack_gitlab_ctl_reconfigure]\""
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-db-postgres-archive]\""
  data_disk_size        = 4000
  data_disk_type        = "pd-ssd"
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  ip_cidr_range         = var.subnetworks["db-dr-archive"]
  machine_type          = var.machine_types["db-dr"]
  name                  = "postgres-dr-archive"
  node_count            = "1"
  project               = var.project
  public_ports          = var.public_ports["db-dr"]
  region                = var.region
  service_account_email = var.service_account_email
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor.git?ref=v3.0.0"
  tier                  = "db"
  use_new_node_name     = true
  vpc                   = module.network.self_link
  os_disk_size          = 100
}

module "postgres-dr-delayed" {
  assign_public_ip      = false
  bootstrap_version     = var.bootstrap_script_version
  chef_init_run_list    = "\"recipe[gitlab-server::hack_gitlab_ctl_reconfigure]\""
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-db-postgres-delayed]\""
  data_disk_size        = 4000
  data_disk_type        = "pd-ssd"
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  ip_cidr_range         = var.subnetworks["db-dr-delayed"]
  machine_type          = var.machine_types["db-dr"]
  name                  = "postgres-dr-delayed"
  node_count            = "1"
  project               = var.project
  public_ports          = var.public_ports["db-dr"]
  region                = var.region
  service_account_email = var.service_account_email
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor.git?ref=v3.0.0"
  tier                  = "db"
  use_new_node_name     = true
  vpc                   = module.network.self_link
  os_disk_size          = 100
}

module "postgres-backup" {
  environment    = var.environment
  backup_readers = ["serviceAccount:${var.gcs_postgres_restore_service_account}"]
  backup_writers = ["serviceAccount:${var.gcs_postgres_backup_service_account}"]
  kms_key_id     = var.gcs_postgres_backup_kms_key_id
  source         = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/database-backup-bucket.git?ref=v3.0.0"
  retention_days = var.postgres_backup_retention_days
}

#############################################
#
#  GCP Internal TCP LoadBalancer and PgBouncer
#
#############################################

module "gcp-tcp-lb-internal-pgbouncer" {
  backend_service        = module.pg-bouncer.google_compute_region_backend_service_self_link
  environment            = var.environment
  external               = false
  forwarding_port_ranges = ["6432"]
  fqdns                  = var.lb_fqdns_internal_pgbouncer
  gitlab_zone_id         = var.gitlab_net_zone_id
  health_check_ports     = ["8010"]
  instances              = module.pg-bouncer.instances_self_link
  lb_count               = "1"
  name                   = "gcp-tcp-lb-internal-pgbouncer"
  names                  = ["${var.environment}-pgbouncer"]
  project                = var.project
  region                 = var.region
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/tcp-lb.git?ref=v2.0.0"
  subnetwork_self_link   = module.pg-bouncer.google_compute_subnetwork_self_link
  targets                = ["pgbouncer"]
  vpc                    = module.network.self_link
}

module "pg-bouncer" {
  assign_public_ip       = false
  backend_service_type   = "regional"
  bootstrap_version      = var.bootstrap_script_version
  chef_init_run_list     = "\"recipe[gitlab-server::hack_gitlab_ctl_reconfigure]\""
  chef_provision         = var.chef_provision
  chef_run_list          = "\"role[${var.environment}-base-db-pgbouncer]\""
  create_backend_service = true
  dns_zone_name          = var.dns_zone_name
  environment            = var.environment
  health_check           = "http"
  health_check_port      = "8010"
  ip_cidr_range          = var.subnetworks["pgb"]
  machine_type           = var.machine_types["pgb"]
  name                   = "pgbouncer"
  node_count             = var.node_count["pgb"]
  project                = var.project
  public_ports           = var.public_ports["pgb"]
  region                 = var.region
  service_account_email  = var.service_account_email
  service_path           = "/"
  service_port           = 6432
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.0.0"
  tier                   = "db"
  use_new_node_name      = true
  vpc                    = module.network.self_link
}

#############################################
#
#  GCP Internal TCP LoadBalancer and Patroni
#
#############################################

module "gcp-tcp-lb-internal-patroni" {
  backend_service        = module.patroni.google_compute_region_backend_service_self_link
  environment            = var.environment
  external               = false
  forwarding_port_ranges = ["6432"]
  fqdns                  = var.lb_fqdns_internal_patroni
  gitlab_zone_id         = var.gitlab_net_zone_id
  health_check_ports     = ["8009"]
  instances              = module.patroni.instances_self_link
  lb_count               = var.node_count["patroni"] > 0 ? 1 : 0
  name                   = "gcp-tcp-lb-internal-patroni"
  names                  = ["${var.environment}-patroni"]
  project                = var.project
  region                 = var.region
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/tcp-lb.git?ref=v2.0.0"
  subnetwork_self_link   = module.patroni.google_compute_subnetwork_self_link
  targets                = ["patroni"]
  vpc                    = module.network.self_link
}

module "patroni" {
  assign_public_ip       = false
  backend_service_type   = "regional"
  bootstrap_version      = var.bootstrap_script_version
  chef_provision         = var.chef_provision
  chef_run_list          = "\"role[${var.environment}-base-db-patroni]\""
  create_backend_service = true
  data_disk_size         = var.data_disk_sizes["patroni"]
  data_disk_type         = "pd-ssd"
  dns_zone_name          = var.dns_zone_name
  environment            = var.environment
  health_check           = "http"
  health_check_port      = "8009"
  ip_cidr_range          = var.subnetworks["patroni"]
  machine_type           = var.machine_types["patroni"]
  name                   = "patroni"
  node_count             = var.node_count["patroni"]
  project                = var.project
  public_ports           = var.public_ports["patroni"]
  region                 = var.region
  service_account_email  = var.service_account_email
  service_path           = "/"
  service_port           = 6432
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor-with-group.git?ref=v3.0.0"
  tier                   = "db"
  use_new_node_name      = true
  vpc                    = module.network.self_link
  os_disk_size           = 100
}

###########################################################
#
#  GCP Internal TCP LoadBalancer and Patroni for PG11
#
###########################################################

module "gcp-tcp-lb-internal-postgres-11" {
  backend_service        = module.patroni-pg-11.google_compute_region_backend_service_self_link
  environment            = var.environment
  external               = false
  forwarding_port_ranges = ["6432"]
  fqdns                  = var.lb_fqdns_internal_postgres_11
  gitlab_zone_id         = var.gitlab_net_zone_id
  health_check_ports     = ["8009"]
  instances              = module.patroni-pg-11.instances_self_link
  lb_count               = var.node_count["postgres-11"] > 0 ? 1 : 0
  name                   = "gcp-tcp-lb-internal-postgres-11"
  names                  = ["${var.environment}-postgres-11"]
  project                = var.project
  region                 = var.region
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/tcp-lb.git?ref=v2.0.0"
  subnetwork_self_link   = module.patroni-pg-11.google_compute_subnetwork_self_link
  targets                = ["postgres-11"]
  vpc                    = module.network.self_link
}

module "patroni-pg-11" {
  assign_public_ip       = false
  backend_service_type   = "regional"
  bootstrap_version      = var.bootstrap_script_version
  chef_provision         = var.chef_provision
  chef_run_list          = "\"role[${var.environment}-base-db-postgres-11]\""
  create_backend_service = true
  data_disk_size         = var.data_disk_sizes["patroni"]
  data_disk_type         = "pd-ssd"
  dns_zone_name          = var.dns_zone_name
  environment            = var.environment
  health_check           = "http"
  health_check_port      = "8009"
  ip_cidr_range          = var.subnetworks["postgres11"]
  machine_type           = var.machine_types["patroni"]
  name                   = "postgres11"
  node_count             = var.node_count["postgres-11"]
  project                = var.project
  public_ports           = var.public_ports["patroni"]
  region                 = var.region
  service_account_email  = var.service_account_email
  service_path           = "/"
  service_port           = 6432
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor-with-group.git?ref=v3.0.0"
  tier                   = "db"
  use_new_node_name      = true
  vpc                    = module.network.self_link
  os_disk_size           = 100
}

##################################
#
#  Redis
#
##################################

module "redis" {
  assign_public_ip          = false
  allow_stopping_for_update = true
  bootstrap_version         = var.bootstrap_script_version
  chef_provision            = var.chef_provision
  chef_run_list             = "\"role[${var.environment}-base-db-redis-server-single]\""
  data_disk_size            = 52
  data_disk_type            = "pd-ssd"
  dns_zone_name             = var.dns_zone_name
  egress_ports              = var.egress_ports
  environment               = var.environment
  ip_cidr_range             = var.subnetworks["redis"]
  machine_type              = var.machine_types["redis"]
  name                      = "redis"
  node_count                = var.node_count["redis"]
  project                   = var.project
  public_ports              = var.public_ports["redis"]
  region                    = var.region
  service_account_email     = var.service_account_email
  source                    = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor.git?ref=v3.0.0"
  tier                      = "db"
  use_new_node_name         = true
  vpc                       = module.network.self_link
}

module "redis-sidekiq" {
  assign_public_ip          = false
  allow_stopping_for_update = true
  bootstrap_version         = var.bootstrap_script_version
  chef_provision            = var.chef_provision
  chef_run_list             = "\"role[${var.environment}-base-db-redis-server-sidekiq]\""
  data_disk_size            = 52
  data_disk_type            = "pd-ssd"
  dns_zone_name             = var.dns_zone_name
  egress_ports              = var.egress_ports
  environment               = var.environment
  ip_cidr_range             = var.subnetworks["redis-sidekiq"]
  machine_type              = var.machine_types["redis-sidekiq"]
  name                      = "redis-sidekiq"
  node_count                = var.node_count["redis-sidekiq"]
  project                   = var.project
  public_ports              = var.public_ports["redis-sidekiq"]
  region                    = var.region
  service_account_email     = var.service_account_email
  source                    = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor.git?ref=v3.0.0"
  tier                      = "db"
  use_new_node_name         = true
  vpc                       = module.network.self_link
}

module "redis-cache" {
  assign_public_ip        = false
  bootstrap_version       = var.bootstrap_script_version
  chef_provision          = var.chef_provision
  dns_zone_name           = var.dns_zone_name
  egress_ports            = var.egress_ports
  environment             = var.environment
  ip_cidr_range           = var.subnetworks["redis-cache"]
  name                    = "redis-cache"
  project                 = var.project
  public_ports            = var.public_ports["redis-cache"]
  redis_chef_run_list     = "\"role[${var.environment}-base-db-redis-server-cache]\""
  redis_count             = var.node_count["redis-cache"]
  redis_data_disk_size    = 100
  redis_data_disk_type    = "pd-ssd"
  redis_machine_type      = var.machine_types["redis-cache"]
  region                  = var.region
  sentinel_chef_run_list  = "\"role[${var.environment}-base-db-redis-sentinel-cache]\""
  sentinel_count          = var.node_count["redis-cache-sentinel"]
  sentinel_data_disk_size = 100
  sentinel_data_disk_type = "pd-ssd"
  sentinel_machine_type   = var.machine_types["redis-cache-sentinel"]
  service_account_email   = var.service_account_email
  source                  = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor-redis.git?ref=v2.1.1"
  tier                    = "db"
  use_new_node_name       = true
  vpc                     = module.network.self_link
}

##################################
#
#  Sidekiq
#
##################################

module "sidekiq" {
  assign_public_ip                    = false
  allow_stopping_for_update           = true
  bootstrap_version                   = var.bootstrap_script_version
  chef_provision                      = var.chef_provision
  chef_run_list                       = "\"role[${var.environment}-base-be-sidekiq-besteffort]\""
  dns_zone_name                       = var.dns_zone_name
  environment                         = var.environment
  ip_cidr_range                       = var.subnetworks["sidekiq"]
  machine_type                        = var.machine_types["sidekiq-besteffort"]
  name                                = "sidekiq"
  os_disk_type                        = "pd-ssd"
  project                             = var.project
  public_ports                        = var.public_ports["sidekiq"]
  region                              = var.region
  service_account_email               = var.service_account_email
  sidekiq_asap_count                  = var.node_count["sidekiq-asap"]
  sidekiq_asap_instance_type          = var.machine_types["sidekiq-asap"]
  sidekiq_besteffort_count            = var.node_count["sidekiq-besteffort"]
  sidekiq_besteffort_instance_type    = var.machine_types["sidekiq-besteffort"]
  sidekiq_elasticsearch_count         = var.node_count["sidekiq-elasticsearch"]
  sidekiq_elasticsearch_instance_type = var.machine_types["sidekiq-elasticsearch"]
  sidekiq_export_count                = var.node_count["sidekiq-export"]
  sidekiq_export_instance_type        = var.machine_types["sidekiq-export"]
  sidekiq_import_count                = var.node_count["sidekiq-import"]
  sidekiq_import_instance_type        = var.machine_types["sidekiq-import"]
  sidekiq_pages_count                 = var.node_count["sidekiq-pages"]
  sidekiq_pages_instance_type         = var.machine_types["sidekiq-pages"]
  sidekiq_pipeline_count              = var.node_count["sidekiq-pipeline"]
  sidekiq_pipeline_instance_type      = var.machine_types["sidekiq-pipeline"]
  sidekiq_pullmirror_count            = var.node_count["sidekiq-pullmirror"]
  sidekiq_pullmirror_instance_type    = var.machine_types["sidekiq-pullmirror"]
  sidekiq_realtime_count              = var.node_count["sidekiq-realtime"]
  sidekiq_realtime_instance_type      = var.machine_types["sidekiq-realtime"]
  source                              = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-sidekiq.git?ref=v4.0.0"
  tier                                = "sv"
  use_new_node_name                   = true
  vpc                                 = module.network.self_link
}

##################################
#
#  Mailroom
#
##################################

module "mailroom" {
  assign_public_ip      = false
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-be-mailroom]\""
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["mailroom"]
  machine_type          = var.machine_types["mailroom"]
  name                  = "mailroom"
  node_count            = var.node_count["mailroom"]
  os_disk_type          = "pd-ssd"
  project               = var.project
  public_ports          = var.public_ports["mailroom"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.0.0"
  tier                  = "sv"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Gitaly Praefect nodes
#
##################################

module "praefect" {
  assign_public_ip      = false
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-be-praefect]\""
  dns_zone_name         = var.dns_zone_name
  egress_ports          = var.egress_ports
  environment           = var.environment
  ip_cidr_range         = var.subnetworks["praefect"]
  machine_type          = var.machine_types["praefect"]
  name                  = "praefect"
  node_count            = var.node_count["praefect"]
  project               = var.project
  public_ports          = var.public_ports["praefect"]
  region                = var.region
  service_account_email = var.service_account_email
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.0.0"
  tier                  = "sv"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Storage nodes for repositories
#
##################################

module "file" {
  assign_public_ip      = false
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-stor-gitaly]\""
  deletion_protection   = true
  data_disk_size        = var.data_disk_sizes["file"]
  data_disk_type        = "pd-ssd"
  dns_zone_name         = var.dns_zone_name
  egress_ports          = var.egress_ports
  environment           = var.environment
  ip_cidr_range         = var.subnetworks["stor"]
  machine_type          = var.machine_types["stor"]
  name                  = "file"
  node_count            = var.node_count["stor"]
  multizone_node_count  = var.node_count["multizone-stor"]
  os_disk_type          = "pd-ssd"
  project               = var.project
  public_ports          = var.public_ports["stor"]
  region                = var.region
  service_account_email = var.service_account_email
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor.git?ref=v3.0.0"
  tier                  = "stor"
  use_new_node_name     = true
  vpc                   = module.network.self_link
  zone                  = "us-east1-c"
}

module "file-zfs" {
  assign_public_ip      = false
  bootstrap_data_disk   = false
  bootstrap_version     = 9
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-stor-gitaly-zfs]\""
  data_disk_name        = "data"
  data_disk_size        = var.data_disk_sizes["file"]
  data_disk_type        = "pd-ssd"
  deletion_protection   = true
  dns_zone_name         = var.dns_zone_name
  egress_ports          = var.egress_ports
  environment           = var.environment
  ip_cidr_range         = var.subnetworks["stor-zfs"]
  local_ssd_count       = 2
  log_disk_size         = 10
  machine_type          = var.machine_types["stor"]
  name                  = "file-zfs"
  node_count            = var.node_count["stor-zfs"]
  os_disk_type          = "pd-ssd"
  project               = var.project
  public_ports          = var.public_ports["stor"]
  region                = var.region
  service_account_email = var.service_account_email
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor.git?ref=v3.0.0"
  tier                  = "stor"
  use_new_node_name     = true
  vpc                   = "gstg"
}

##################################
#
#  Storage nodes for
#  uploads/lfs/pages/artifacts/builds/cache
#
#  share:
#    gitlab-ci/builds
#    gitlab-rails/shared/cache
#    gitlab-rails/shared/tmp
#    gitlab-rails/uploads
#    gitlab-rails/shared/lfs-objects
#    gitlab-rails/shared/artifacts
#
#  pages:
#    gitlab-rails/shared/pages
#
##################################

module "share" {
  assign_public_ip      = false
  bootstrap_version     = var.bootstrap_script_version
  deletion_protection   = true
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-stor-nfs-server]\""
  data_disk_size        = var.data_disk_sizes["share"]
  data_disk_type        = "pd-ssd"
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  egress_ports          = var.egress_ports
  ip_cidr_range         = var.subnetworks["share"]
  machine_type          = var.machine_types["stor-share"]
  name                  = "share"
  node_count            = var.node_count["share"]
  os_disk_type          = "pd-ssd"
  project               = var.project
  public_ports          = var.public_ports["stor"]
  region                = var.region
  service_account_email = var.service_account_email
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor.git?ref=v3.0.0"
  tier                  = "stor"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

module "pages" {
  assign_public_ip      = false
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-stor-nfs-server]\""
  deletion_protection   = true
  data_disk_size        = var.data_disk_sizes["pages"]
  data_disk_type        = "pd-ssd"
  dns_zone_name         = var.dns_zone_name
  egress_ports          = var.egress_ports
  environment           = var.environment
  ip_cidr_range         = var.subnetworks["pages"]
  machine_type          = var.machine_types["stor-pages"]
  name                  = "pages"
  node_count            = var.node_count["pages"]
  os_disk_type          = "pd-ssd"
  project               = var.project
  public_ports          = var.public_ports["stor"]
  region                = var.region
  service_account_email = var.service_account_email
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor.git?ref=v3.0.0"
  tier                  = "stor"
  use_new_node_name     = true
  use_external_ip       = false
  vpc                   = module.network.self_link
}

##################################
#
#  External HAProxy LoadBalancer
#
##################################

module "fe-lb" {
  assign_public_ip       = false
  backend_service_type   = "regional"
  bootstrap_version      = var.bootstrap_script_version
  chef_provision         = var.chef_provision
  chef_run_list          = "\"role[${var.environment}-base-lb-fe]\""
  create_backend_service = true
  dns_zone_name          = var.dns_zone_name
  environment            = var.environment
  health_check           = "http"
  ip_cidr_range          = var.subnetworks["fe-lb"]
  machine_type           = var.machine_types["fe-lb"]
  name                   = "fe"
  node_count             = var.node_count["fe-lb"]
  os_boot_image          = var.os_boot_image["fe-lb"]
  project                = var.project
  public_ports           = var.public_ports["fe-lb"]
  region                 = var.region
  service_account_email  = var.service_account_email
  service_path           = "/-/available-https"
  service_port           = 8002
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.0.0"
  tier                   = "lb"
  use_new_node_name      = true
  vpc                    = module.network.self_link
}

##################################
#
#  External HAProxy LoadBalancer Pages
#
##################################

module "fe-lb-pages" {
  assign_public_ip      = false
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-lb-pages]\""
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  health_check          = "http"
  ip_cidr_range         = var.subnetworks["fe-lb-pages"]
  machine_type          = var.machine_types["fe-lb"]
  name                  = "fe-pages"
  node_count            = var.node_count["fe-lb-pages"]
  project               = var.project
  public_ports          = var.public_ports["fe-lb"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 7331
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.0.0"
  tier                  = "lb"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  External HAProxy LoadBalancer AltSSH
#
##################################

module "fe-lb-altssh" {
  assign_public_ip      = false
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-lb-altssh]\""
  dns_zone_name         = var.dns_zone_name
  egress_ports          = var.egress_ports
  environment           = var.environment
  health_check          = "http"
  ip_cidr_range         = var.subnetworks["fe-lb-altssh"]
  machine_type          = var.machine_types["fe-lb"]
  name                  = "fe-altssh"
  node_count            = var.node_count["fe-lb-altssh"]
  project               = var.project
  public_ports          = var.public_ports["fe-lb"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 7331
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.0.0"
  tier                  = "lb"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  External HAProxy LoadBalancer Registry
#
##################################

module "fe-lb-registry" {
  assign_public_ip       = false
  bootstrap_version      = var.bootstrap_script_version
  chef_provision         = var.chef_provision
  chef_run_list          = "\"role[${var.environment}-base-lb-registry]\""
  create_backend_service = true
  dns_zone_name          = var.dns_zone_name
  environment            = var.environment
  health_check           = "http"
  ip_cidr_range          = var.subnetworks["fe-lb-registry"]
  machine_type           = var.machine_types["fe-lb"]
  name                   = "fe-registry"
  node_count             = var.node_count["fe-lb-registry"]
  project                = var.project
  public_ports           = var.public_ports["fe-lb"]
  region                 = var.region
  service_account_email  = var.service_account_email
  service_path           = "/-/available-https"
  service_port           = 8002
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.0.0"
  tier                   = "lb"
  use_new_node_name      = true
  vpc                    = module.network.self_link
}

##################################
#
#  External HAProxy LoadBalancer Canary
#
##################################

module "fe-lb-cny" {
  assign_public_ip       = false
  bootstrap_version      = var.bootstrap_script_version
  chef_provision         = var.chef_provision
  chef_run_list          = "\"role[${var.environment}-base-lb-cny]\""
  create_backend_service = true
  dns_zone_name          = var.dns_zone_name
  environment            = var.environment
  health_check           = "http"
  ip_cidr_range          = var.subnetworks["fe-lb-cny"]
  machine_type           = var.machine_types["fe-lb"]
  name                   = "fe-cny"
  node_count             = var.node_count["fe-lb-cny"]
  project                = var.project
  public_ports           = var.public_ports["fe-lb"]
  region                 = var.region
  service_account_email  = var.service_account_email
  service_path           = "/-/available-https"
  service_port           = 8002
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.0.0"
  tier                   = "lb"
  use_new_node_name      = true
  vpc                    = module.network.self_link
}

##################################
#
#  GCP TCP LoadBalancers
#
##################################

#### Load balancer for the main site
module "gcp-tcp-lb" {
  environment            = var.environment
  forwarding_port_ranges = var.tcp_lbs["forwarding_port_ranges"]
  fqdns                  = var.lb_fqdns
  gitlab_zone_id         = var.gitlab_com_zone_id
  health_check_ports     = var.tcp_lbs["health_check_ports"]
  instances              = module.fe-lb.instances_self_link
  lb_count               = length(var.tcp_lbs["names"])
  name                   = "gcp-tcp-lb"
  names                  = var.tcp_lbs["names"]
  project                = var.project
  region                 = var.region
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/tcp-lb.git?ref=v2.0.0"
  targets                = ["fe"]
}

##################################
#
#  GCP Internal TCP LoadBalancers
#
##################################

###### Internal Load balancer for the main site
module "gcp-tcp-lb-internal" {
  backend_service        = module.fe-lb.google_compute_region_backend_service_self_link
  environment            = var.environment
  external               = false
  forwarding_port_ranges = var.tcp_lbs_internal["forwarding_port_ranges"]
  fqdns                  = var.lb_fqdns_internal
  gitlab_zone_id         = var.gitlab_net_zone_id
  health_check_ports     = var.tcp_lbs_internal["health_check_ports"]
  instances              = module.fe-lb.instances_self_link
  lb_count               = length(var.tcp_lbs_internal["names"])
  name                   = "gcp-tcp-lb-internal"
  names                  = var.tcp_lbs_internal["names"]
  project                = var.project
  region                 = var.region
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/tcp-lb.git?ref=v2.0.0"
  subnetwork_self_link   = module.fe-lb.google_compute_subnetwork_self_link
  targets                = ["fe"]
  vpc                    = module.network.self_link
}

#### Load balancer for pages
module "gcp-tcp-lb-pages" {
  environment            = var.environment
  forwarding_port_ranges = var.tcp_lbs_pages["forwarding_port_ranges"]
  fqdns                  = var.lb_fqdns_pages
  gitlab_zone_id         = var.gitlab_io_zone_id
  health_check_ports     = var.tcp_lbs_pages["health_check_ports"]
  instances              = module.fe-lb-pages.instances_self_link
  lb_count               = length(var.tcp_lbs_pages["names"])
  name                   = "gcp-tcp-lb-pages"
  names                  = var.tcp_lbs_pages["names"]
  project                = var.project
  region                 = var.region
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/tcp-lb.git?ref=v2.0.0"
  targets                = ["fe-pages"]
}

#### Load balancer for altssh
module "gcp-tcp-lb-altssh" {
  environment                = var.environment
  forwarding_port_ranges     = var.tcp_lbs_altssh["forwarding_port_ranges"]
  fqdns                      = var.lb_fqdns_altssh
  gitlab_zone_id             = var.gitlab_com_zone_id
  health_check_ports         = var.tcp_lbs_altssh["health_check_ports"]
  health_check_request_paths = var.tcp_lbs_altssh["health_check_request_paths"]
  instances                  = module.fe-lb-altssh.instances_self_link
  lb_count                   = length(var.tcp_lbs_altssh["names"])
  name                       = "gcp-tcp-lb-altssh"
  names                      = var.tcp_lbs_altssh["names"]
  project                    = var.project
  region                     = var.region
  source                     = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/tcp-lb.git?ref=v2.0.0"
  targets                    = ["fe-altssh"]
}

#### Load balancer for registry
module "gcp-tcp-lb-registry" {
  environment            = var.environment
  forwarding_port_ranges = var.tcp_lbs_registry["forwarding_port_ranges"]
  fqdns                  = var.lb_fqdns_registry
  gitlab_zone_id         = var.gitlab_com_zone_id
  health_check_ports     = var.tcp_lbs_registry["health_check_ports"]
  instances              = module.fe-lb-registry.instances_self_link
  lb_count               = length(var.tcp_lbs_registry["names"])
  name                   = "gcp-tcp-lb-registry"
  names                  = var.tcp_lbs_registry["names"]
  project                = var.project
  region                 = var.region
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/tcp-lb.git?ref=v2.0.0"
  targets                = ["fe-registry"]
}

#### Load balancer for cny
module "gcp-tcp-lb-cny" {
  environment            = var.environment
  forwarding_port_ranges = var.tcp_lbs_cny["forwarding_port_ranges"]
  fqdns                  = var.lb_fqdns_cny
  gitlab_zone_id         = var.gitlab_com_zone_id
  health_check_ports     = var.tcp_lbs_cny["health_check_ports"]
  instances              = module.fe-lb-cny.instances_self_link
  lb_count               = length(var.tcp_lbs_cny["names"])
  name                   = "gcp-tcp-lb-cny"
  names                  = var.tcp_lbs_cny["names"]
  project                = var.project
  region                 = var.region
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/tcp-lb.git?ref=v2.0.0"
  targets                = ["fe-cny"]
}

#### Load balancer for bastion
module "gcp-tcp-lb-bastion" {
  environment            = var.environment
  forwarding_port_ranges = var.tcp_lbs_bastion["forwarding_port_ranges"]
  fqdns                  = var.lb_fqdns_bastion
  gitlab_zone_id         = var.gitlab_com_zone_id
  health_check_ports     = var.tcp_lbs_bastion["health_check_ports"]
  instances              = module.bastion.instances_self_link
  lb_count               = length(var.tcp_lbs_bastion["names"])
  name                   = "gcp-tcp-lb-bastion"
  names                  = var.tcp_lbs_bastion["names"]
  project                = var.project
  region                 = var.region
  session_affinity       = "CLIENT_IP"
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/tcp-lb.git?ref=v2.0.0"
  targets                = ["bastion"]
}

##################################
#
#  Consul
#
##################################

module "consul" {
  assign_public_ip      = false
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-infra-consul]\""
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  ip_cidr_range         = var.subnetworks["consul"]
  machine_type          = var.machine_types["consul"]
  name                  = "consul"
  node_count            = var.node_count["consul"]
  project               = var.project
  public_ports          = var.public_ports["consul"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 8300
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.0.0"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Pubsubbeats
#
#  Machines for running the beats
#  that consume logs from pubsub
#  and send them to elastic cloud
#
#  You must have a chef role with the
#  following format:
#     role[<env>-infra-pubsubbeat-<beat_name>]
#
##################################

module "pubsubbeat" {
  assign_public_ip      = false
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  dns_zone_name         = var.dns_zone_name
  egress_ports          = var.egress_ports
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["pubsubbeat"]
  machine_types         = var.pubsubbeats["machine_types"]
  names                 = var.pubsubbeats["names"]
  project               = var.project
  public_ports          = var.public_ports["pubsubbeat"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/pubsubbeat.git?ref=v3.0.0"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Monitoring
#
#  Uses the monitoring module, this
#  creates a single instance behind
#  a load balancer with identity aware
#  proxy enabled.
#
##################################

resource "google_compute_subnetwork" "monitoring" {
  ip_cidr_range            = var.subnetworks["monitoring"]
  enable_flow_logs         = false
  name                     = format("monitoring-%v", var.environment)
  network                  = module.network.self_link
  private_ip_google_access = true
  project                  = var.project
  region                   = var.region
}

#######################
#
# load balancer for all hosts in this section
#
#######################

module "monitoring-lb" {
  cert_link       = var.monitoring_cert_link
  environment     = var.environment
  dns_zone_id     = var.gitlab_net_zone_id
  dns_zone_name   = "gstg.gitlab.net"
  hosts           = var.monitoring_hosts["names"]
  name            = "monitoring-lb"
  project         = var.project
  region          = var.region
  service_ports   = var.monitoring_hosts["ports"]
  source          = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/https-lb.git?ref=v2.0.0"
  subnetwork_name = google_compute_subnetwork.monitoring.name
  targets         = var.monitoring_hosts["names"]
  url_map         = google_compute_url_map.monitoring-lb.self_link
}

#######################
module "prometheus" {
  assign_public_ip  = true
  bootstrap_version = var.bootstrap_script_version
  chef_provision    = var.chef_provision
  chef_run_list     = "\"role[${var.environment}-infra-prometheus]\""
  data_disk_size    = var.data_disk_sizes["prometheus"]
  data_disk_type    = "pd-ssd"
  dns_zone_name     = var.dns_zone_name
  environment       = var.environment
  fw_whitelist_subnets = concat(
    var.monitoring_whitelist_prometheus["subnets"],
    var.other_monitoring_subnets,
  )
  fw_whitelist_ports    = var.monitoring_whitelist_prometheus["ports"]
  machine_type          = var.machine_types["monitoring"]
  name                  = "prometheus"
  node_count            = var.node_count["prometheus"]
  oauth2_client_id      = var.oauth2_client_id_monitoring
  oauth2_client_secret  = var.oauth2_client_secret_monitoring
  persistent_disk_path  = "/opt/prometheus"
  project               = var.project
  region                = var.region
  service_account_email = var.service_account_email
  service_path          = "/graph"
  service_port = element(
    var.monitoring_hosts["ports"],
    index(var.monitoring_hosts["names"], "prometheus"),
  )
  source            = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/monitoring-with-count.git?ref=v3.0.0"
  subnetwork_name   = google_compute_subnetwork.monitoring.name
  tier              = "inf"
  use_external_ip   = true
  use_new_node_name = true
  vpc               = module.network.self_link
}

module "prometheus-app" {
  assign_public_ip  = false
  bootstrap_version = var.bootstrap_script_version
  chef_provision    = var.chef_provision
  chef_run_list     = "\"role[${var.environment}-infra-prometheus-app]\""
  data_disk_size    = var.data_disk_sizes["prometheus"]
  data_disk_type    = "pd-ssd"
  dns_zone_name     = var.dns_zone_name
  environment       = var.environment
  fw_whitelist_subnets = concat(
    var.monitoring_whitelist_prometheus["subnets"],
    var.other_monitoring_subnets,
  )
  fw_whitelist_ports    = var.monitoring_whitelist_prometheus["ports"]
  machine_type          = var.machine_types["monitoring"]
  name                  = "prometheus-app"
  node_count            = var.node_count["prometheus-app"]
  oauth2_client_id      = var.oauth2_client_id_monitoring
  oauth2_client_secret  = var.oauth2_client_secret_monitoring
  persistent_disk_path  = "/opt/prometheus"
  project               = var.project
  region                = var.region
  service_account_email = var.service_account_email
  service_path          = "/graph"
  service_port = element(
    var.monitoring_hosts["ports"],
    index(var.monitoring_hosts["names"], "prometheus-app"),
  )
  source            = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/monitoring-with-count.git?ref=v3.0.0"
  subnetwork_name   = google_compute_subnetwork.monitoring.name
  tier              = "inf"
  use_external_ip   = false
  use_new_node_name = true
  vpc               = module.network.self_link
}

module "prometheus-db" {
  assign_public_ip  = false
  bootstrap_version = var.bootstrap_script_version
  chef_provision    = var.chef_provision
  chef_run_list     = "\"role[${var.environment}-infra-prometheus-db]\""
  data_disk_size    = var.data_disk_sizes["prometheus"]
  data_disk_type    = "pd-ssd"
  dns_zone_name     = var.dns_zone_name
  environment       = var.environment
  fw_whitelist_subnets = concat(
    var.monitoring_whitelist_prometheus["subnets"],
    var.other_monitoring_subnets,
  )
  fw_whitelist_ports    = var.monitoring_whitelist_prometheus["ports"]
  machine_type          = var.machine_types["monitoring"]
  name                  = "prometheus-db"
  node_count            = var.node_count["prometheus-db"]
  oauth2_client_id      = var.oauth2_client_id_monitoring
  oauth2_client_secret  = var.oauth2_client_secret_monitoring
  persistent_disk_path  = "/opt/prometheus"
  project               = var.project
  region                = var.region
  service_account_email = var.service_account_email
  service_path          = "/graph"
  service_port = element(
    var.monitoring_hosts["ports"],
    index(var.monitoring_hosts["names"], "prometheus-db"),
  )
  source            = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/monitoring-with-count.git?ref=v3.0.0"
  subnetwork_name   = google_compute_subnetwork.monitoring.name
  tier              = "inf"
  use_external_ip   = false
  use_new_node_name = true
  vpc               = module.network.self_link
}

module "alerts" {
  assign_public_ip  = false
  bootstrap_version = var.bootstrap_script_version
  chef_provision    = var.chef_provision
  chef_run_list     = "\"role[${var.environment}-infra-alerts]\""
  data_disk_size    = 100
  data_disk_type    = "pd-standard"
  dns_zone_name     = var.dns_zone_name
  environment       = var.environment
  fw_whitelist_subnets = concat(
    var.monitoring_whitelist_alerts["subnets"],
    var.other_monitoring_subnets,
  )
  fw_whitelist_ports    = var.monitoring_whitelist_alerts["ports"]
  health_check          = "tcp"
  machine_type          = var.machine_types["alerts"]
  name                  = "alerts"
  node_count            = var.node_count["alerts"]
  oauth2_client_id      = var.oauth2_client_id_monitoring
  oauth2_client_secret  = var.oauth2_client_secret_monitoring
  persistent_disk_path  = "/opt"
  project               = var.project
  region                = var.region
  service_account_email = var.service_account_email
  service_port = element(
    var.monitoring_hosts["ports"],
    index(var.monitoring_hosts["names"], "alerts"),
  )
  source            = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/monitoring-with-count.git?ref=v3.0.0"
  subnetwork_name   = google_compute_subnetwork.monitoring.name
  tier              = "inf"
  use_new_node_name = true
  vpc               = module.network.self_link
}

module "sd-exporter" {
  assign_public_ip          = false
  additional_scopes         = ["https://www.googleapis.com/auth/monitoring"]
  allow_stopping_for_update = true
  bootstrap_version         = var.bootstrap_script_version
  chef_provision            = var.chef_provision
  chef_run_list             = "\"role[${var.environment}-infra-sd-exporter]\""
  create_backend_service    = false
  dns_zone_name             = var.dns_zone_name
  environment               = var.environment
  machine_type              = var.machine_types["sd-exporter"]
  name                      = "sd-exporter"
  node_count                = var.node_count["sd-exporter"]
  project                   = var.project
  public_ports              = var.public_ports["sd-exporter"]
  region                    = var.region
  service_account_email     = var.service_account_email
  source                    = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.0.0"
  subnetwork_name           = google_compute_subnetwork.monitoring.name
  tier                      = "inf"
  use_new_node_name         = true
  vpc                       = module.network.self_link
}

module "blackbox" {
  assign_public_ip      = false
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-blackbox]\""
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  health_check          = "tcp"
  machine_type          = var.machine_types["blackbox"]
  name                  = "blackbox"
  node_count            = var.node_count["blackbox"]
  project               = var.project
  public_ports          = var.public_ports["blackbox"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.0.0"
  subnetwork_name       = google_compute_subnetwork.monitoring.name
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = module.network.self_link
  use_external_ip       = false
}

module "influxdb" {
  assign_public_ip      = false
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-influxdb]\""
  data_disk_size        = 1000
  data_disk_type        = "pd-ssd"
  dns_zone_name         = var.dns_zone_name
  egress_ports          = var.egress_ports
  environment           = var.environment
  ip_cidr_range         = var.subnetworks["influxdb"]
  machine_type          = var.machine_types["influxdb"]
  monitoring_whitelist  = var.monitoring_whitelist_influxdb
  name                  = "influxdb"
  node_count            = var.node_count["influxdb"]
  project               = var.project
  public_ports          = var.public_ports["influxdb"]
  region                = var.region
  service_account_email = var.service_account_email
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor.git?ref=v3.0.0"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

module "thanos-store" {
  allow_stopping_for_update = true
  assign_public_ip          = false
  bootstrap_version         = var.bootstrap_script_version
  chef_provision            = var.chef_provision
  chef_run_list             = "\"role[${var.environment}-infra-thanos-store]\""
  data_disk_size            = 100
  data_disk_type            = "pd-ssd"
  dns_zone_name             = var.dns_zone_name
  egress_ports              = var.egress_ports
  environment               = var.environment
  ip_cidr_range             = var.subnetworks["thanos-store"]
  machine_type              = var.machine_types["thanos-store"]
  monitoring_whitelist      = var.monitoring_whitelist_thanos
  name                      = "thanos-store"
  node_count                = var.node_count["thanos-store"]
  persistent_disk_path      = "/opt/prometheus"
  project                   = var.project
  public_ports              = var.public_ports["thanos"]
  region                    = var.region
  service_account_email     = var.service_account_email
  source                    = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor.git?ref=v3.0.0"
  tier                      = "inf"
  use_new_node_name         = true
  vpc                       = module.network.self_link
}

module "thanos-compact" {
  allow_stopping_for_update = true
  assign_public_ip          = false
  bootstrap_version         = var.bootstrap_script_version
  chef_provision            = var.chef_provision
  chef_run_list             = "\"role[${var.environment}-infra-thanos-compact]\""
  data_disk_size            = 500
  data_disk_type            = "pd-ssd"
  dns_zone_name             = var.dns_zone_name
  egress_ports              = var.egress_ports
  environment               = var.environment
  ip_cidr_range             = var.subnetworks["thanos-compact"]
  machine_type              = var.machine_types["thanos-compact"]
  monitoring_whitelist      = var.monitoring_whitelist_thanos
  name                      = "thanos-compact"
  node_count                = var.node_count["thanos-compact"]
  persistent_disk_path      = "/opt/prometheus"
  project                   = var.project
  public_ports              = var.public_ports["thanos"]
  region                    = var.region
  service_account_email     = var.service_account_email
  source                    = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor.git?ref=v3.0.0"
  tier                      = "inf"
  use_new_node_name         = true
  vpc                       = module.network.self_link
}

##################################
#
#  Console
#
##################################

module "console" {
  assign_public_ip      = false
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-console-node]\""
  dns_zone_name         = var.dns_zone_name
  egress_ports          = var.console_egress_ports
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["console"]
  machine_type          = var.machine_types["console"]
  name                  = "console"
  node_count            = var.node_count["console"]
  project               = var.project
  public_ports          = var.public_ports["console"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.0.0"
  tier                  = "sv"
  use_external_ip       = false
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

module "console-migrate-shard" {
  assign_public_ip      = false
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-console-node]\""
  dns_zone_name         = var.dns_zone_name
  egress_ports          = var.console_egress_ports
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["console-migrate-shard"]
  machine_type          = var.machine_types["console-migrate-shard"]
  name                  = "console-migrate-shard"
  node_count            = 1
  project               = var.project
  public_ports          = var.public_ports["console"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.0.0"
  tier                  = "sv"
  use_external_ip       = false
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Deploy
#
##################################

module "deploy" {
  assign_public_ip      = false
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-deploy-node]\""
  dns_zone_name         = var.dns_zone_name
  egress_ports          = var.deploy_egress_ports
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["deploy"]
  machine_type          = var.machine_types["deploy"]
  name                  = "deploy"
  node_count            = var.node_count["deploy"]
  project               = var.project
  public_ports          = var.public_ports["deploy"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.0.0"
  tier                  = "sv"
  use_external_ip       = false
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Runner
#
##################################

module "runner" {
  assign_public_ip      = false
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-runner]\""
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["runner"]
  machine_type          = var.machine_types["runner"]
  name                  = "runner"
  node_count            = var.node_count["runner"]
  project               = var.project
  public_ports          = var.public_ports["runner"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.0.0"
  tier                  = "sv"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Bastion
#
##################################

module "bastion" {
  assign_public_ip      = false
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-bastion]\""
  dns_zone_name         = var.dns_zone_name
  egress_ports          = var.egress_ports
  environment           = var.environment
  health_check          = "http"
  ip_cidr_range         = var.subnetworks["bastion"]
  machine_type          = var.machine_types["bastion"]
  name                  = "bastion"
  node_count            = var.node_count["bastion"]
  project               = var.project
  public_ports          = var.public_ports["bastion"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.0.0"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Object storage buckets
#
##################################

module "gitlab_object_storage" {
  environment                       = var.environment
  service_account_email             = var.service_account_email
  gcs_service_account_email         = var.gcs_service_account_email
  gcs_storage_analytics_group_email = var.gcs_storage_analytics_group_email
  source                            = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/storage-buckets.git?ref=v5.0.0"
}

resource "google_kms_key_ring_iam_binding" "bootstrap" {
  key_ring_id = "${var.project}/global/gitlab-${var.environment}-bootstrap"
  role        = "roles/cloudkms.cryptoKeyDecrypter"

  members = [
    "serviceAccount:${var.service_account_email}",
  ]
}

resource "google_kms_key_ring_iam_binding" "secrets" {
  key_ring_id = "${var.project}/global/gitlab-secrets"
  role        = "roles/cloudkms.cryptoKeyDecrypter"

  members = [
    "serviceAccount:${var.service_account_email}",
  ]
}

##################################
#
#  Registry Analytics. Needed temporarily for
#  https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/5149
#
##################################

module "registry-analytics" {
  assign_public_ip      = false
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-registry-analytics]\""
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["registry-analytics"]
  machine_type          = var.machine_types["registry-analytics"]
  name                  = "registry-analytics"
  node_count            = var.node_count["registry-analytics"]
  os_disk_type          = "pd-ssd"
  project               = var.project
  public_ports          = var.public_ports["registry-analytics"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.0.0"
  tier                  = "sv"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

############################
# Stackdriver log exclusions
############################

module "stackdriver" {
  sd_log_filters = var.sd_log_filters
  source         = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/stackdriver.git?ref=v2.0.0"
}

############################
# Camo proxy
############################

module "camoproxy-lb" {
  cert_link       = var.camoproxy_cert_link
  dns_zone_id     = var.gitlab_static_net_zone_id
  dns_zone_name   = var.camoproxy_domain
  environment     = var.environment
  hosts           = [var.camoproxy_hostname]
  name            = "camo-proxy"
  project         = var.project
  region          = var.region
  service_ports   = [var.camoproxy_serviceport]
  source          = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/https-lb.git?ref=v2.0.0"
  subnetwork_name = module.camoproxy.google_compute_subnetwork_name
  targets         = ["camoproxy"]
  url_map         = google_compute_url_map.camoproxy-lb.self_link
}

module "camoproxy" {
  assign_public_ip      = false
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-svc-camoproxy]\""
  dns_zone_name         = var.dns_zone_name
  egress_ports          = var.camoproxy_egress_ports
  environment           = var.environment
  health_check          = "http"
  ip_cidr_range         = var.subnetworks["camoproxy"]
  log_disk_size         = 20
  machine_type          = var.machine_types["camoproxy"]
  name                  = "camoproxy"
  node_count            = var.node_count["camoproxy"]
  os_boot_image         = var.os_boot_image["camoproxy"]
  project               = var.project
  region                = var.region
  service_account_email = var.service_account_email
  service_path          = "/healthcheck"
  service_port          = var.camoproxy_serviceport
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.0.0"
  tier                  = "sv"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

# Deny all egress to RFC1918 (internal) IP addresses.  The camo proxy must not be able to go direct to any other internal places
resource "google_compute_firewall" "camoproxy-deny-internal-egress" {
  name    = format("%v-camoproxy-deny-internal", var.environment)
  network = var.environment

  priority  = "500" # must come before any other standard allows (typically priority 1000)
  direction = "EGRESS"

  deny {
    protocol = "all"
  }

  destination_ranges = ["10.0.0.0/8", "172.16.0.0/12", "192.168.0.0/16"]

  target_tags = ["camoproxy"]
}

#Simple url map; everything goes to the one backend
resource "google_compute_url_map" "camoproxy-lb" {
  name            = format("%v-camoproxy-lb", var.environment)
  default_service = module.camoproxy.google_compute_backend_service_self_link
}

##################################
#
#  GKE Cluster for gstg GitLab services
#
##################################

# Service account utilized by CI
resource "google_service_account" "k8s-workloads" {
  account_id   = "k8s-workloads"
  display_name = "k8s-workloads"
}

# Reserved IP address for container registry
resource "google_compute_address" "registry-gke" {
  name         = "registry-gke-${var.environment}"
  description  = "registry-gke-${var.environment}"
  subnetwork   = module.gitlab-gke.subnetwork_self_link
  address_type = "INTERNAL"
}

# Reserved IP address for prometheus operator
resource "google_compute_address" "prometheus-gke" {
  name         = "prometheus-gke-${var.environment}"
  description  = "prometheus-gke-${var.environment}"
  subnetwork   = module.gitlab-gke.subnetwork_self_link
  address_type = "INTERNAL"
}

# Reserved IP address for thanos-query
resource "google_compute_address" "thanos-query-gke" {
  name         = "thanos-query-gke-${var.environment}"
  description  = "thanos-query-gke-${var.environment}"
  subnetwork   = module.gitlab-gke.subnetwork_self_link
  address_type = "INTERNAL"
}

# Reserved IP address for plantuml, must be global

resource "google_compute_global_address" "plantuml-gke" {
  name        = "plantuml-gke-${var.environment}"
  description = "plantuml-gke-${var.environment}"
}

resource "aws_route53_record" "plantuml-gke" {
  zone_id = var.gitlab_static_net_zone_id
  name    = "gstg.plantuml.gitlab-static.net"
  type    = "A"
  ttl     = "300"
  records = [google_compute_global_address.plantuml-gke.address]
}

module "gitlab-gke" {
  environment = var.environment
  name        = "gitlab-gke"
  vpc         = module.network.self_link
  source      = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/gke.git?ref=v7.0.0"

  authorized_master_access = [
    "35.231.92.80/32",   # console-01-sv-gstg
    "35.227.116.144/32", # ops.gitlab.net
    "35.185.18.176/32",  # runner-01-inf-ops
  ]

  ip_cidr_range          = var.subnetworks["gitlab-gke"]
  disable_network_policy = "false"
  dns_zone_name          = var.dns_zone_name
  kubernetes_version     = "1.14.7-gke.10"
  node_network_policy    = "true"
  private_cluster        = "true"
  private_master_cidr    = var.master_cidr_subnets["gitlab-gke"]
  project                = var.project
  region                 = var.region
  pod_ip_cidr_range      = var.subnetworks["gitlab-gke-pod-cidr"]
  service_ip_cidr_range  = var.subnetworks["gitlab-gke-service-cidr"]

  node_pools = [
    {
      name               = "node-pool-20190926-0"
      initial_node_count = "1"
      machine_type       = var.machine_types["gitlab-gke"]
      max_node_count     = "6"
      node_auto_repair   = "true"
      node_auto_upgrade  = "false"
      node_disk_size_gb  = "50"
      node_disk_type     = "pd-standard"
      preemptible        = "true"
    },
  ]
}

resource "google_logging_project_sink" "pubsub" {
  name = "${var.environment}-pubsub-sink"

  destination = "pubsub.googleapis.com/projects/${var.project}/topics/${module.pubsubbeat.topic_names[21]}"
  filter      = "resource.labels.cluster_name = ${var.environment}-gitlab-gke"

  unique_writer_identity = true
  depends_on             = [module.pubsubbeat]
}

resource "google_project_iam_binding" "log-writer" {
  role = "roles/pubsub.publisher"

  members = [
    google_logging_project_sink.pubsub.writer_identity,
    "serviceAccount:${var.service_account_email}",
  ]
}

resource "google_compute_firewall" "consul-deny-internal" {
  name    = format("%v-consul-deny-internal", var.environment)
  network = var.environment

  priority  = "500" # must come before any other standard allows (typically priority 1000)
  direction = "INGRESS"

  deny {
    protocol = "tcp"

    ports = [
      "8500",
    ]
  }
}
