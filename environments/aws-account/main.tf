// Configure remote state
terraform {
  backend "s3" {
    bucket = "gitlab-com-infrastructure"
    key    = "terraform/aws-account/terraform.tfstate"
    region = "us-east-1"
  }
}

// Use credentials from environment or shared credentials file
provider "aws" {
  region  = "us-east-1"
  version = "~> 2.27"
}

module "cloudtrail" {
  source                        = "git::https://github.com/cloudposse/terraform-aws-cloudtrail.git?ref=0.9.0"
  namespace                     = "gitlab"
  stage                         = "aws"
  name                          = "cloudtrail-default"
  enable_logging                = "true"
  enable_log_file_validation    = "true"
  include_global_service_events = "true"
  is_multi_region_trail         = "true"
  s3_bucket_name                = module.cloudtrail_s3_bucket.bucket_id
}

module "cloudtrail_s3_bucket" {
  source    = "git::https://github.com/cloudposse/terraform-aws-cloudtrail-s3-bucket.git?ref=0.1.1"
  namespace = "gitlab"
  stage     = "aws"
  name      = "cloudtrail-default"
  region    = "us-east-1"
}

resource "aws_s3_bucket" "forum_backup" {
  bucket = "gitlab-discourse-forum-backup"
  acl    = "private"
}

resource "aws_iam_user" "forum_backup" {
  name = "forum-backup"
}

resource "aws_iam_access_key" "forum_backup" {
  user = aws_iam_user.forum_backup.name
}

resource "aws_iam_policy" "write_forum_backup" {
  name = "write-forum-backup"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": "s3:*",
      "Resource": [
        "arn:aws:s3:::${aws_s3_bucket.forum_backup.bucket}",
        "arn:aws:s3:::${aws_s3_bucket.forum_backup.bucket}/*"
      ]
    }
  ]
}
EOF

}

resource "aws_iam_user_policy_attachment" "forum_backup" {
  user       = aws_iam_user.forum_backup.name
  policy_arn = aws_iam_policy.write_forum_backup.arn
}

