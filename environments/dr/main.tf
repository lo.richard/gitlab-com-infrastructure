## State storage
terraform {
  backend "s3" {
    bucket = "gitlab-com-infrastructure"
    key    = "terraform/dr/terraform.tfstate"
    region = "us-east-1"
  }
}

## AWS
provider "aws" {
  region  = "us-west-1"
  version = "~> 2.27.0"
}

## Local
provider "local" {
  version = "~> 1.3.0"
}

## Google

provider "google" {
  project = var.project
  region  = var.region
  version = "~> 2.14.0"
}

resource "google_project_iam_member" "terraform" {
  count   = length(local.gce_vm_sa_roles)
  project = var.project
  role    = element(local.gce_vm_sa_roles, count.index)
  member  = "serviceAccount:${var.service_account_email}"
}

/*
##################################
#
#  NAT gateway
#
#################################
module "nat" {
  source     = "GoogleCloudPlatform/nat-gateway/google"
  region     = "${var.region}"
  network    = "${var.environment}"
}
*/
##################################
#
#  Network
#
#################################

module "network" {
  environment      = var.environment
  project          = var.project
  source           = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/vpc.git?ref=v2.0.0"
  internal_subnets = var.internal_subnets
}

##################################
#
#  Network Peering
#
#################################

resource "google_compute_network_peering" "peering" {
  count        = length(var.peer_networks["names"])
  name         = "peering-${element(var.peer_networks["names"], count.index)}"
  network      = var.network_env
  peer_network = element(var.peer_networks["links"], count.index)
}

##################################
#
#  Web front-end
#
#################################

module "web" {
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-fe-web]\""
  dns_zone_name         = var.dns_zone_name
  egress_ports          = var.egress_ports
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["web"]
  machine_type          = var.machine_types["web"]
  name                  = "web"
  node_count            = var.node_count["web"]
  os_disk_type          = "pd-ssd"
  project               = var.project
  public_ports          = var.public_ports["web"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 443
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.0.0"
  tier                  = "sv"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  API
#
#################################

module "api" {
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-fe-api]\""
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["api"]
  machine_type          = var.machine_types["api"]
  name                  = "api"
  node_count            = var.node_count["api"]
  os_disk_type          = "pd-ssd"
  project               = var.project
  public_ports          = var.public_ports["api"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 443
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.0.0"
  tier                  = "sv"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Git
#
##################################

module "git" {
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-fe-git]\""
  dns_zone_name         = var.dns_zone_name
  egress_ports          = var.egress_ports
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["git"]
  machine_type          = var.machine_types["git"]
  name                  = "git"
  node_count            = var.node_count["git"]
  os_disk_type          = "pd-ssd"
  project               = var.project
  public_ports          = var.public_ports["git"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.0.0"
  tier                  = "sv"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

#############################################
#
#  GCP Internal TCP LoadBalancer and Patroni
#
#############################################

module "gcp-tcp-lb-internal-patroni" {
  backend_service        = module.patroni.google_compute_region_backend_service_self_link
  environment            = var.environment
  external               = false
  forwarding_port_ranges = ["6432"]
  fqdns                  = var.lb_fqdns_internal_patroni
  gitlab_zone_id         = var.gitlab_net_zone_id
  health_check_ports     = ["8009"]
  instances              = module.patroni.instances_self_link
  lb_count               = var.node_count["patroni"] > 0 ? 1 : 0
  name                   = "gcp-tcp-lb-internal-patroni"
  names                  = ["${var.environment}-patroni"]
  project                = var.project
  region                 = var.region
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/tcp-lb.git?ref=v2.0.0"
  subnetwork_self_link   = module.patroni.google_compute_subnetwork_self_link
  targets                = ["patroni"]
  vpc                    = module.network.self_link
}

module "patroni" {
  backend_service_type   = "regional"
  bootstrap_version      = var.bootstrap_script_version
  chef_provision         = var.chef_provision
  chef_run_list          = "\"role[${var.environment}-base-db-patroni]\""
  create_backend_service = true
  data_disk_size         = var.data_disk_sizes["patroni"]
  data_disk_type         = "pd-ssd"
  dns_zone_name          = var.dns_zone_name
  environment            = var.environment
  health_check           = "http"
  health_check_port      = "8009"
  ip_cidr_range          = var.subnetworks["patroni"]
  machine_type           = var.machine_types["db"]
  name                   = "patroni"
  node_count             = var.node_count["patroni"]
  project                = var.project
  public_ports           = var.public_ports["db"]
  region                 = var.region
  service_account_email  = var.service_account_email
  service_path           = "/"
  service_port           = 6432
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor-with-group.git?ref=v3.0.0"
  tier                   = "db"
  use_new_node_name      = true
  vpc                    = module.network.self_link
  os_disk_size           = 100
}

#############################################
module "geo-postgres" {
  bootstrap_version     = var.bootstrap_script_version
  chef_init_run_list    = "\"recipe[gitlab-server::hack_gitlab_ctl_reconfigure]\""
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-db-geo-postgres]\""
  data_disk_size        = 5000
  data_disk_type        = "pd-ssd"
  dns_zone_name         = var.dns_zone_name
  egress_ports          = var.egress_ports
  environment           = var.environment
  ip_cidr_range         = var.subnetworks["geodb"]
  machine_type          = var.machine_types["geodb"]
  name                  = "geo-postgres"
  node_count            = var.node_count["geodb"]
  project               = var.project
  public_ports          = var.public_ports["geodb"]
  region                = var.region
  service_account_email = var.service_account_email
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor.git?ref=v3.0.0"
  tier                  = "db"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Redis
#
##################################

module "redis" {
  allow_stopping_for_update = true
  bootstrap_version         = var.bootstrap_script_version
  chef_provision            = var.chef_provision
  chef_run_list             = "\"role[${var.environment}-base-db-redis]\""
  data_disk_size            = 52
  data_disk_type            = "pd-standard"
  dns_zone_name             = var.dns_zone_name
  egress_ports              = var.egress_ports
  environment               = var.environment
  ip_cidr_range             = var.subnetworks["redis"]
  machine_type              = var.machine_types["redis"]
  name                      = "redis"
  node_count                = var.node_count["redis"]
  project                   = var.project
  public_ports              = var.public_ports["redis"]
  region                    = var.region
  service_account_email     = var.service_account_email
  source                    = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor.git?ref=v3.0.0"
  tier                      = "db"
  use_new_node_name         = true
  vpc                       = module.network.self_link
}

##################################
#
#  Sidekiq
#
##################################

module "sidekiq" {
  bootstrap_version                   = var.bootstrap_script_version
  chef_provision                      = var.chef_provision
  chef_run_list                       = "\"role[${var.environment}-base-be-sidekiq]\""
  dns_zone_name                       = var.dns_zone_name
  environment                         = var.environment
  ip_cidr_range                       = var.subnetworks["sidekiq"]
  machine_type                        = var.machine_types["sidekiq"]
  name                                = "sidekiq"
  os_disk_type                        = "pd-ssd"
  project                             = var.project
  public_ports                        = var.public_ports["sidekiq"]
  region                              = var.region
  service_account_email               = var.service_account_email
  sidekiq_asap_count                  = var.node_count["sidekiq-asap"]
  sidekiq_asap_instance_type          = var.machine_types["sidekiq-asap"]
  sidekiq_besteffort_count            = var.node_count["sidekiq-besteffort"]
  sidekiq_besteffort_instance_type    = var.machine_types["sidekiq-besteffort"]
  sidekiq_elasticsearch_count         = var.node_count["sidekiq-elasticsearch"]
  sidekiq_elasticsearch_instance_type = var.machine_types["sidekiq-elasticsearch"]
  sidekiq_export_count                = var.node_count["sidekiq-export"]
  sidekiq_export_instance_type        = var.machine_types["sidekiq-export"]
  sidekiq_import_count                = var.node_count["sidekiq-import"]
  sidekiq_import_instance_type        = var.machine_types["sidekiq-import"]
  sidekiq_pages_count                 = var.node_count["sidekiq-pages"]
  sidekiq_pages_instance_type         = var.machine_types["sidekiq-pages"]
  sidekiq_pipeline_count              = var.node_count["sidekiq-pipeline"]
  sidekiq_pipeline_instance_type      = var.machine_types["sidekiq-pipeline"]
  sidekiq_pullmirror_count            = var.node_count["sidekiq-pullmirror"]
  sidekiq_pullmirror_instance_type    = var.machine_types["sidekiq-pullmirror"]
  sidekiq_realtime_count              = var.node_count["sidekiq-realtime"]
  sidekiq_realtime_instance_type      = var.machine_types["sidekiq-realtime"]
  source                              = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-sidekiq.git?ref=v4.0.0"
  tier                                = "sv"
  use_new_node_name                   = true
  vpc                                 = module.network.self_link
}

##################################
#
#  Storage nodes for repositories
#
##################################

module "file" {
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-stor-gitaly]\""
  data_disk_size        = var.data_disk_sizes["file"]
  data_disk_type        = "pd-standard"
  dns_zone_name         = var.dns_zone_name
  egress_ports          = var.egress_ports
  environment           = var.environment
  ip_cidr_range         = var.subnetworks["stor"]
  machine_type          = var.machine_types["stor"]
  name                  = "file"
  node_count            = var.node_count["stor"]
  multizone_node_count  = var.node_count["multizone-stor"]
  os_disk_type          = "pd-ssd"
  project               = var.project
  public_ports          = var.public_ports["stor"]
  region                = var.region
  service_account_email = var.service_account_email
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor.git?ref=v3.0.0"
  tier                  = "stor"
  use_new_node_name     = true
  vpc                   = module.network.self_link
  zone                  = "us-west1-c"
}

##################################
#
#  Storage nodes for
#  uploads/lfs/pages/artifacts/builds/cache
#
#  pages:
#    gitlab-rails/shared/pages
#
##################################

module "pages" {
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-stor-nfs-server]\""
  data_disk_size        = var.data_disk_sizes["pages"]
  data_disk_type        = "pd-ssd"
  dns_zone_name         = var.dns_zone_name
  egress_ports          = var.egress_ports
  environment           = var.environment
  ip_cidr_range         = var.subnetworks["pages"]
  machine_type          = var.machine_types["stor-pages"]
  name                  = "pages"
  node_count            = var.node_count["pages"]
  os_disk_type          = "pd-ssd"
  project               = var.project
  public_ports          = var.public_ports["stor"]
  region                = var.region
  service_account_email = var.service_account_email
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor.git?ref=v3.0.0"
  tier                  = "stor"
  use_new_node_name     = true
  use_external_ip       = true
  vpc                   = module.network.self_link
}

##################################
#
#  External HAProxy LoadBalancer
#
##################################

module "fe-lb" {
  backend_service_type   = "regional"
  bootstrap_version      = var.bootstrap_script_version
  chef_provision         = var.chef_provision
  chef_run_list          = "\"role[${var.environment}-base-lb-fe]\""
  create_backend_service = true
  dns_zone_name          = var.dns_zone_name
  environment            = var.environment
  health_check           = "http"
  ip_cidr_range          = var.subnetworks["fe-lb"]
  machine_type           = var.machine_types["fe-lb"]
  name                   = "fe"
  node_count             = var.node_count["fe-lb"]
  project                = var.project
  public_ports           = var.public_ports["fe-lb"]
  region                 = var.region
  service_account_email  = var.service_account_email
  service_path           = "/-/available-https"
  service_port           = 8002
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.0.0"
  tier                   = "lb"
  use_new_node_name      = true
  vpc                    = module.network.self_link
}

##################################
#
#  External HAProxy LoadBalancer AltSSH
#
##################################

module "fe-lb-altssh" {
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-lb-altssh]\""
  dns_zone_name         = var.dns_zone_name
  egress_ports          = var.egress_ports
  environment           = var.environment
  health_check          = "http"
  ip_cidr_range         = var.subnetworks["fe-lb-altssh"]
  machine_type          = var.machine_types["fe-lb"]
  name                  = "fe-altssh"
  node_count            = var.node_count["fe-lb-altssh"]
  project               = var.project
  public_ports          = var.public_ports["fe-lb"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 7331
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.0.0"
  tier                  = "lb"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  GCP TCP LoadBalancers
#
##################################

#### Load balancer for the main site
module "gcp-tcp-lb" {
  environment            = var.environment
  forwarding_port_ranges = var.tcp_lbs["forwarding_port_ranges"]
  fqdns                  = var.lb_fqdns
  gitlab_zone_id         = var.gitlab_com_zone_id
  health_check_ports     = var.tcp_lbs["health_check_ports"]
  instances              = module.fe-lb.instances_self_link
  lb_count               = length(var.tcp_lbs["names"])
  name                   = "gcp-tcp-lb"
  names                  = var.tcp_lbs["names"]
  project                = var.project
  region                 = var.region
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/tcp-lb.git?ref=v2.0.0"
  targets                = ["fe"]
}

##################################
#
#  GCP Internal TCP LoadBalancers
#
##################################

###### Internal Load balancer for the main site
module "gcp-tcp-lb-internal" {
  backend_service        = module.fe-lb.google_compute_region_backend_service_self_link
  environment            = var.environment
  external               = false
  forwarding_port_ranges = var.tcp_lbs_internal["forwarding_port_ranges"]
  fqdns                  = var.lb_fqdns_internal
  gitlab_zone_id         = var.gitlab_net_zone_id
  health_check_ports     = var.tcp_lbs_internal["health_check_ports"]
  instances              = module.fe-lb.instances_self_link
  lb_count               = length(var.tcp_lbs_internal["names"])
  name                   = "gcp-tcp-lb-internal"
  names                  = var.tcp_lbs_internal["names"]
  project                = var.project
  region                 = var.region
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/tcp-lb.git?ref=v2.0.0"
  subnetwork_self_link   = module.fe-lb.google_compute_subnetwork_self_link
  targets                = ["fe"]
  vpc                    = module.network.self_link
}

#### Load balancer for altssh
module "gcp-tcp-lb-altssh" {
  environment                = var.environment
  forwarding_port_ranges     = var.tcp_lbs_altssh["forwarding_port_ranges"]
  fqdns                      = var.lb_fqdns_altssh
  gitlab_zone_id             = var.gitlab_com_zone_id
  health_check_ports         = var.tcp_lbs_altssh["health_check_ports"]
  health_check_request_paths = var.tcp_lbs_altssh["health_check_request_paths"]
  instances                  = module.fe-lb-altssh.instances_self_link
  lb_count                   = length(var.tcp_lbs_altssh["names"])
  name                       = "gcp-tcp-lb-altssh"
  names                      = var.tcp_lbs_altssh["names"]
  project                    = var.project
  region                     = var.region
  source                     = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/tcp-lb.git?ref=v2.0.0"
  targets                    = ["fe-altssh"]
}

#### Load balancer for bastion
module "gcp-tcp-lb-bastion" {
  environment            = var.environment
  forwarding_port_ranges = var.tcp_lbs_bastion["forwarding_port_ranges"]
  fqdns                  = var.lb_fqdns_bastion
  gitlab_zone_id         = var.gitlab_com_zone_id
  health_check_ports     = var.tcp_lbs_bastion["health_check_ports"]
  instances              = module.bastion.instances_self_link
  lb_count               = length(var.tcp_lbs_bastion["names"])
  name                   = "gcp-tcp-lb-bastion"
  names                  = var.tcp_lbs_bastion["names"]
  project                = var.project
  region                 = var.region
  session_affinity       = "CLIENT_IP"
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/tcp-lb.git?ref=v2.0.0"
  targets                = ["bastion"]
}

##################################
#
#  Consul
#
##################################

module "consul" {
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-infra-consul]\""
  dns_zone_name         = var.dns_zone_name
  environment           = var.environment
  ip_cidr_range         = var.subnetworks["consul"]
  machine_type          = var.machine_types["consul"]
  name                  = "consul"
  node_count            = var.node_count["consul"]
  project               = var.project
  public_ports          = var.public_ports["consul"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 8300
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.0.0"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Pubsubbeats
#
#  Machines for running the beats
#  that consume logs from pubsub
#  and send them to elastic cloud
#
#  You must have a chef role with the
#  following format:
#     role[<env>-infra-pubsubbeat-<beat_name>]
#
##################################

module "pubsubbeat" {
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  dns_zone_name         = var.dns_zone_name
  egress_ports          = var.egress_ports
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["pubsubbeat"]
  machine_types         = var.pubsubbeats["machine_types"]
  names                 = var.pubsubbeats["names"]
  project               = var.project
  public_ports          = var.public_ports["pubsubbeat"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/pubsubbeat.git?ref=v3.0.0"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Monitoring
#
#  Uses the monitoring module, this
#  creates a single instance behind
#  a load balancer with identity aware
#  proxy enabled.
#
##################################

resource "google_compute_subnetwork" "monitoring" {
  ip_cidr_range            = var.subnetworks["monitoring"]
  enable_flow_logs         = false
  name                     = format("monitoring-%v", var.environment)
  network                  = module.network.self_link
  private_ip_google_access = true
  project                  = var.project
  region                   = var.region
}

#######################
#
# load balancer for all hosts in this section
#
#######################

module "monitoring-lb" {
  cert_link       = var.monitoring_cert_link
  environment     = var.environment
  dns_zone_id     = var.gitlab_net_zone_id
  dns_zone_name   = "dr.gitlab.net"
  hosts           = var.monitoring_hosts["names"]
  name            = "monitoring-lb"
  project         = var.project
  region          = var.region
  service_ports   = var.monitoring_hosts["ports"]
  source          = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/https-lb.git?ref=v2.0.0"
  subnetwork_name = google_compute_subnetwork.monitoring.name
  targets         = var.monitoring_hosts["names"]
  url_map         = google_compute_url_map.monitoring-lb.self_link
}

#######################
module "prometheus" {
  bootstrap_version = var.bootstrap_script_version
  chef_provision    = var.chef_provision
  chef_run_list     = "\"role[${var.environment}-infra-prometheus]\""
  data_disk_size    = 50
  data_disk_type    = "pd-ssd"
  dns_zone_name     = var.dns_zone_name
  environment       = var.environment
  fw_whitelist_subnets = concat(
    var.monitoring_whitelist_prometheus["subnets"],
    var.other_monitoring_subnets,
  )
  fw_whitelist_ports    = var.monitoring_whitelist_prometheus["ports"]
  machine_type          = var.machine_types["monitoring"]
  name                  = "prometheus"
  node_count            = var.node_count["prometheus"]
  oauth2_client_id      = var.oauth2_client_id_monitoring
  oauth2_client_secret  = var.oauth2_client_secret_monitoring
  persistent_disk_path  = "/opt/prometheus"
  project               = var.project
  region                = var.region
  service_account_email = var.service_account_email
  service_path          = "/graph"
  service_port = element(
    var.monitoring_hosts["ports"],
    index(var.monitoring_hosts["names"], "prometheus"),
  )
  source            = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/monitoring-with-count.git?ref=v3.0.0"
  subnetwork_name   = google_compute_subnetwork.monitoring.name
  tier              = "inf"
  use_external_ip   = true
  use_new_node_name = true
  vpc               = module.network.self_link
}

module "prometheus-app" {
  bootstrap_version = var.bootstrap_script_version
  chef_provision    = var.chef_provision
  chef_run_list     = "\"role[${var.environment}-infra-prometheus-app]\""
  data_disk_size    = 50
  data_disk_type    = "pd-ssd"
  dns_zone_name     = var.dns_zone_name
  environment       = var.environment
  fw_whitelist_subnets = concat(
    var.monitoring_whitelist_prometheus["subnets"],
    var.other_monitoring_subnets,
  )
  fw_whitelist_ports    = var.monitoring_whitelist_prometheus["ports"]
  machine_type          = var.machine_types["monitoring"]
  name                  = "prometheus-app"
  node_count            = var.node_count["prometheus-app"]
  oauth2_client_id      = var.oauth2_client_id_monitoring
  oauth2_client_secret  = var.oauth2_client_secret_monitoring
  persistent_disk_path  = "/opt/prometheus"
  project               = var.project
  region                = var.region
  service_account_email = var.service_account_email
  service_path          = "/graph"
  service_port = element(
    var.monitoring_hosts["ports"],
    index(var.monitoring_hosts["names"], "prometheus-app"),
  )
  source            = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/monitoring-with-count.git?ref=v3.0.0"
  subnetwork_name   = google_compute_subnetwork.monitoring.name
  tier              = "inf"
  use_external_ip   = true
  use_new_node_name = true
  vpc               = module.network.self_link
}

module "sd-exporter" {
  additional_scopes         = ["https://www.googleapis.com/auth/monitoring"]
  allow_stopping_for_update = true
  bootstrap_version         = var.bootstrap_script_version
  chef_provision            = var.chef_provision
  chef_run_list             = "\"role[${var.environment}-infra-sd-exporter]\""
  create_backend_service    = false
  dns_zone_name             = var.dns_zone_name
  environment               = var.environment
  machine_type              = var.machine_types["sd-exporter"]
  name                      = "sd-exporter"
  node_count                = var.node_count["sd-exporter"]
  project                   = var.project
  public_ports              = var.public_ports["sd-exporter"]
  region                    = var.region
  service_account_email     = var.service_account_email
  source                    = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.0.0"
  subnetwork_name           = google_compute_subnetwork.monitoring.name
  tier                      = "inf"
  use_new_node_name         = true
  vpc                       = module.network.self_link
}

module "thanos-store" {
  allow_stopping_for_update = true
  bootstrap_version         = var.bootstrap_script_version
  chef_provision            = var.chef_provision
  chef_run_list             = "\"role[${var.environment}-infra-thanos-store]\""
  data_disk_size            = 100
  data_disk_type            = "pd-ssd"
  dns_zone_name             = var.dns_zone_name
  egress_ports              = var.egress_ports
  environment               = var.environment
  ip_cidr_range             = var.subnetworks["thanos-store"]
  machine_type              = var.machine_types["thanos-store"]
  monitoring_whitelist      = var.monitoring_whitelist_thanos
  name                      = "thanos-store"
  node_count                = var.node_count["thanos-store"]
  persistent_disk_path      = "/opt/prometheus"
  project                   = var.project
  public_ports              = var.public_ports["thanos"]
  region                    = var.region
  service_account_email     = var.service_account_email
  source                    = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor.git?ref=v3.0.0"
  tier                      = "inf"
  use_new_node_name         = true
  vpc                       = module.network.self_link
}

module "thanos-compact" {
  allow_stopping_for_update = true
  bootstrap_version         = var.bootstrap_script_version
  chef_provision            = var.chef_provision
  chef_run_list             = "\"role[${var.environment}-infra-thanos-compact]\""
  data_disk_size            = 100
  data_disk_type            = "pd-ssd"
  dns_zone_name             = var.dns_zone_name
  egress_ports              = var.egress_ports
  environment               = var.environment
  ip_cidr_range             = var.subnetworks["thanos-compact"]
  machine_type              = var.machine_types["thanos-compact"]
  monitoring_whitelist      = var.monitoring_whitelist_thanos
  name                      = "thanos-compact"
  node_count                = var.node_count["thanos-compact"]
  persistent_disk_path      = "/opt/prometheus"
  project                   = var.project
  public_ports              = var.public_ports["thanos"]
  region                    = var.region
  service_account_email     = var.service_account_email
  source                    = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-stor.git?ref=v3.0.0"
  tier                      = "inf"
  use_new_node_name         = true
  vpc                       = module.network.self_link
}

##################################
#
#  Console
#
##################################

module "console" {
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-console-node]\""
  dns_zone_name         = var.dns_zone_name
  egress_ports          = var.egress_ports
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["console"]
  machine_type          = var.machine_types["console"]
  name                  = "console"
  node_count            = var.node_count["console"]
  project               = var.project
  public_ports          = var.public_ports["console"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.0.0"
  tier                  = "sv"
  use_external_ip       = true
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Deploy
#
##################################

module "deploy" {
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-deploy-node]\""
  dns_zone_name         = var.dns_zone_name
  egress_ports          = var.deploy_egress_ports
  environment           = var.environment
  health_check          = "tcp"
  ip_cidr_range         = var.subnetworks["deploy"]
  machine_type          = var.machine_types["deploy"]
  name                  = "deploy"
  node_count            = var.node_count["deploy"]
  project               = var.project
  public_ports          = var.public_ports["deploy"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.0.0"
  tier                  = "sv"
  use_external_ip       = true
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Bastion
#
##################################

module "bastion" {
  bootstrap_version     = var.bootstrap_script_version
  chef_provision        = var.chef_provision
  chef_run_list         = "\"role[${var.environment}-base-bastion]\""
  dns_zone_name         = var.dns_zone_name
  egress_ports          = var.egress_ports
  environment           = var.environment
  health_check          = "http"
  ip_cidr_range         = var.subnetworks["bastion"]
  machine_type          = var.machine_types["bastion"]
  name                  = "bastion"
  node_count            = var.node_count["bastion"]
  project               = var.project
  public_ports          = var.public_ports["bastion"]
  region                = var.region
  service_account_email = var.service_account_email
  service_port          = 22
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v3.0.0"
  tier                  = "inf"
  use_new_node_name     = true
  vpc                   = module.network.self_link
}

##################################
#
#  Object storage buckets
#
##################################

module "gitlab_object_storage" {
  environment                       = var.environment
  artifact_age                      = var.artifact_age
  lfs_object_age                    = var.lfs_object_age
  package_repo_age                  = var.package_repo_age
  upload_age                        = var.upload_age
  storage_log_age                   = var.storage_log_age
  storage_class                     = var.storage_class
  service_account_email             = var.service_account_email
  gcs_service_account_email         = var.gcs_service_account_email
  gcs_storage_analytics_group_email = var.gcs_storage_analytics_group_email
  source                            = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/storage-buckets.git?ref=v5.0.0"
}

resource "google_kms_key_ring_iam_binding" "bootstrap" {
  key_ring_id = "${var.project}/global/gitlab-${var.environment}-bootstrap"
  role        = "roles/cloudkms.cryptoKeyDecrypter"

  members = [
    "serviceAccount:${var.service_account_email}",
  ]
}

resource "google_kms_key_ring_iam_binding" "secrets" {
  key_ring_id = "${var.project}/global/gitlab-secrets"
  role        = "roles/cloudkms.cryptoKeyDecrypter"

  members = [
    "serviceAccount:${var.service_account_email}",
  ]
}
