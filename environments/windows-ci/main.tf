terraform {
  backend "s3" {
    bucket = "gitlab-com-infrastructure"
    key    = "terraform/windows-ci/terraform.tfstate"
    region = "us-east-1"
  }
}

provider "google" {
  project = var.project
  region  = var.region
  version = "~> 2.14.0"
}

# Didn't use the VPC module here because it creates firewall rules we don't want
resource "google_compute_network" "windows-ci-network" {
  name                    = var.network
  auto_create_subnetworks = false
}

# module "nat" {
#   source = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/cloud-nat.git?ref=v0.4.0"
#   nat_ports_per_vm = 256
#   nat_ip_count     = 40
#   network_name     = var.network
#   region           = var.region
# }

resource "google_compute_subnetwork" "manager-subnet" {
  name          = "manager-subnet"
  ip_cidr_range = "10.1.0.0/16"
  region        = var.region
  network       = google_compute_network.windows-ci-network.self_link
}

resource "google_compute_subnetwork" "runner-subnet" {
  name                     = "runner-subnet"
  ip_cidr_range            = "10.2.0.0/16"
  region                   = var.region
  private_ip_google_access = true
  network                  = google_compute_network.windows-ci-network.self_link
}
