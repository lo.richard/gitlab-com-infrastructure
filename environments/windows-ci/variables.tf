variable "project" {
  default = "gitlab-ci-windows"
}

variable "region" {
  default = "us-east1"
}

variable "network" {
  default = "windows-ci"
}

# See 1password
variable "miner_ips" {
  type    = list(string)
  default = []
}

