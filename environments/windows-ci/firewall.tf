

resource "google_compute_firewall" "runners-manager" {
  name        = "runners-manager-to-vms"
  direction   = "INGRESS"
  network     = var.network
  priority    = 1000
  target_tags = ["windows-runner"]

  source_ranges = [
    "10.1.0.0/16"
  ]

  allow {
    protocol = "tcp"
    ports    = ["5985-5986"]
  }
}

resource "google_compute_firewall" "windows-runner-egress" {
  name        = "windows-runner-egress"
  direction   = "EGRESS"
  network     = var.network
  priority    = 1000
  target_tags = ["windows-runner"]

  allow {
    protocol = "tcp"
    ports    = ["80", "443", "22", "1024-65535"]
  }
}

#### Monitoring

resource "google_compute_firewall" "prometheus" {
  name      = "prometheus"
  direction = "INGRESS"
  network   = var.network
  priority  = 1000

  source_ranges = [
    "104.209.180.217/32",
    "104.209.189.215/32",
    "13.77.80.142/32",
    "40.70.72.145/32",
    "13.68.87.12/32",
    "52.225.221.89/32",
    "52.184.190.120/32",
    "35.185.93.139",
    "35.185.46.150",
    "35.227.108.10",
    "35.237.14.194",
    "35.227.109.92",
    "35.237.131.211",
  ]

  allow {
    protocol = "tcp"
    ports    = [9090, 9100, 9402, 9145, 9393, 9000]
  }
}

resource "google_compute_firewall" "thanos" {
  name        = "thanos"
  description = "Prometheus Thanos access"
  direction   = "INGRESS"
  network     = var.network
  priority    = 1000

  target_tags = ["prometheus-server"]

  source_ranges = [
    "35.227.109.92/32",
    "35.237.131.211/32",
    "35.227.203.148/32",
    "130.211.36.217/32",
    "35.237.55.26/32",
    "35.237.254.196/32",
    "104.196.117.149",
  ]

  allow {
    protocol = "tcp"
    ports    = [10901, 10902]
  }
}
