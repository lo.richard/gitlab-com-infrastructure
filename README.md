# GitLab.com Infrastructure

## What is this?

Here you can find the Terraform configuration for GitLab.com, the staging environment and possibly something else.

## What is Terraform?

[Terraform](https://www.terraform.io) is an infrastructure-as-code tool that greatly reduces the amount of time needed to implement and scale our infrastructure. It's provider agnostic so it works great for our use case. You're encouraged to read the documentation as it's very well written. Reading [Terraform: Up and Running](https://www.amazon.com/Terraform-Running-Writing-Infrastructure-Code-ebook/dp/B06XKHGJHP) is also recommended.

## How the repository is structured

Each environment has its own directory.

There is currently one [Terraform state](https://www.terraform.io/docs/state/index.html) for each environment although we're considering to split at least the production one into smaller ones.

Cattle is handled in loops while pets require the full configuration to be
coded. This means you only need to change `count` in `main.tf` to scale a fleet
in and out. More information about [managing cattle can be found in our
docs](./docs/working-with-cattle.md)

### Modules

Modules live in individual repositories in the
[`gitlab-com/gl-infra/terraform-modules`](https://ops.gitlab.net/gitlab-com/gl-infra/terraform-modules)
group in the ops.gitlab.net instance (see
[infrastructure#5688](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/5688)
for current progress).  These standalone modules use automatic semantic
versioning.  See CONTRIBUTING.md in each repo for details; in short, if you
prefix your commit messages with a known magic value (fix:, feat: etc) then when
merged an updated version tag will be pushed based on semver rules, which can
then be used as the ref value of the 'source' attribute of module use in this
repo.

After releasing a new module version, you are encouraged to verify that no
unaccounted for side-effects / traps for the next developer are left in any
environment by bumping the module declaration everywhere, unless there is a good
reason not to. There is a script in `./bin/bump-module` to make this easier.

## Setup (How can I use this?)

First and foremost, remember that Terraform is an extremely powerful tool so be sure you know how to use it well before applying changes in production. Always `plan` and don't hesitate to ask in `#production` if you are in doubt or you see something off in the plan, even the smallest thing.

* [Create a new staging environment](https://drive.google.com/open?id=0BzamLYNnSQa_cjN5NGtaRnpyRXc) (video - internal only)<br>Watch this for a complete overview of how to work with Terraform.

### Making changes

We value small changes.  There are times when we need to make changes across a large number of hosts in the infrastructure.  If possible, break your changes down into smallest subset of functional changes in order to limit the effects of a change at first.  This also helps us keep the master branch `terraform apply`-able.  We have learned from past incidents that it can be hard to untangle big changes.  We may have multiple people working on terraform at the same time so keeping your changes smaller, concise, and more incremental will make all changes easier to incorporate.  

We have a badge at the top - before you make a branch off of master, you will want to make sure that TF plan is clean on the environment you plan to change.  This will hopefully help keep you from getting into a situation where the state file gets complicated.  

### Important notes

* We use differing versions of terraform in our environments.  You'll want to
  ensure you have all available versions prior running anything in terraform.
  * `find ./ -name .terraform-version -exec cat {} \;`
  * our wrapper scripts mentioned above will help protect you from running
    terraform with the incorrect desired version
* Make sure you're not specifying `knife[:identity_file]` in your `.chef/knife.rb`. If you do need it then comment it out every time you create a new node. If you don't then the node provisioning will fail with an error like this: `Errno::ENODEV: Operation not supported by device`.
* Be extremely careful when you initialize the state for the first time. Do not load the variables on staging and then `cd` to production to initialize the state: this would link production to the staging state and you do not want that. It's good practice to initialize the states as soon as you set up your environment to avoid making mistakes, so:
```
cd staging
tf init
cd ../production
tf init
...
```
Then start working.

#### Getting Started:

  1. Clone [chef-repo](https://ops.gitlab.net/gitlab-cookbooks/chef-repo) if you haven't already.
  1. Install a tool for generating passwords that are easier for human beings to remember, like the [xkcd password generator](https://github.com/redacted/XKCD-password-generator) which may be installed with `pip install xkcdpass`, or [pwgen](https://github.com/jbernard/pwgen), which is available in the Ubuntu, Debian, Fedora, Suse, and macOS Homebrew repositories (`apt install --assume-yes pwgen`, `yum install -y pwgen`, `dnf install -y pwgen`, or `brew install pwgen`).
  1. It is highly suggested to utilize some sort of terraform versioning manager such as [tfenv](https://github.com/kamatama41/tfenv) with `brew install tfenv` in order to manage multiple versions of Terraform. So if _for instance_ you want to install version 0.9.8 all you have to do it run `tfenv install 0.9.8` and enable it with `tfenv use 0.9.8`. To verify this, run `terraform -version` and check the version string. See the _Important Notes_ section above to determine which versions are used in our environment.
  1. There is a wrapper script in the `bin/` directory that should be used in place of the `terraform` executable, `tf`. Ensure that `gitlab-com-infrastructure/bin` is in your $PATH
  1. You need to provide some secrets to Terraform using shell env vars. Wrappers in `bin/` source a number of files in `private/env_vars`.
  1. Install the [1password CLI tool](https://1password.com/downloads/command-line/)
  1. Login to 1password with `op signin gitlab user@gitlab.com OP-SECRET-KEY`.
  1. Run `tf-get-secrets` to fetch and update the env var files.
  1. Be sure that the [gcloud-cli is configured and authenticated](https://gitlab.com/gitlab-com/runbooks/blob/master/howto/gcloud-cli.md)
  1. Set up access via [GSTG bastion hosts](https://gitlab.com/gitlab-com/runbooks/blob/master/howto/gstg-bastions.md)
  1. See Important notes section above about initializing terraform. Run `tf init` and `tf plan` in the `environments/gstg` directory and ensure that terraform runs and you can see pending changes

### Creating a new environment

Bootstrapping new GitLab environments is still not fully automated or defined.
This section concerns itself more with setting up a new terraform-able GCP
environment, which can then be used to provision resources to create a new
GitLab environment.

There are 2 parts to this: we must create the GCP resources, which consist of
the project itself and a service account that can create (and eventually) apply
plans in CI. Secondly, we must create AWS resources to act as a terraform
backend.

These instructions currently jump between a few different documents in order to
satisfy different use cases:

1. Creating brand new environments with a new GCP project.
1. Importing existing GCP projects into terraform management.
1. Adding CI planning/applying for GCP projects that are already managed by
   terraform.

Directions:

1. Follow the instructions in
   [environments/env-projects/README.md](environments/env-projects/README.md) to
   create or import a GCP project.
1. In the production GitLab AWS account, create a new IAM user `terraform-$ENV`
   with programmatic access. Direct attach the AmazonS3FullAccess policy.
1. Create a 1password entry "terraform-private/env_vars/$ENV.env" in the
   production vault. It should contain AWS IAM env var declarations for the
   credentials generated in the previous step.
1. In gitlab-com-infrastructure, run `tf-get-secrets` (it's in `./bin`, which is
   always on `$PATH` in order to properly interact with this repo). The new env
   file should be downloaded. You should now be able to run `tf init` in
   `environments/$ENV` and plan/apply.
1. Create [CI env
   vars](https://ops.gitlab.net/gitlab-com/gitlab-com-infrastructure/-/settings/ci_cd)
   for the new environment:

   1. Entries for each var in the `private/env-vars` file, including the AWS
      credentials.
   1. A file entry with key `GCLOUD_TERRAFORM_PRIVATE_KEY_JSON` whose value is
      the contents of the private key file created for the `terraform-ci` user
      previously.

1. Add a section to `.gitlab-ci.yml` for the new environment. Look to existing
   entries for inspiration.
1. CI should now plan (and eventually apply) plans for the new environment.

### Caveats

- Don't use `tf destroy` on cattle unless you **really** know what you're doing. Be aware that deleting an element of an array other than the last one will result in all the resources starting from that element to be destroyed as well. So if you delete `module.something.resource[0]` Terraform will delete **everything** on that array, even if the confirmation dialog only reports the one you want to delete.
- Requesting a quota increase on GCP generally takes an hour or two for memory and CPU increases, but takes several days for increases in SSD and standard disk space.

---

## Contributing

See [CONTRIBUTING.md](./CONTRIBUTING.md).

## License

See [LICENSE](./LICENSE).
